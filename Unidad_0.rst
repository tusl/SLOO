================================================
Software Libre: Ofimática en las Organizaciones 
================================================
--------------------------------------------------------------------
Unidad 0: Introducción a la Computación.
--------------------------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Introducción a la Computación.

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022.

:Autor: Leopoldo Bertinetti


.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

.. contents:: Contenido

.. raw:: pdf

    PageBreak



**Introducción**
===================

Este apunte tiene como objetivo brindar los conocimientos básicos y necesarios para que los alumnos de la materia *Software Libre: Ofimática en las Organizaciones* puedan desarrollar satisfactoriamente el cursado de la misma.


**¿Qué es la computación o informática?**
===========================================

El término **''computación''** o también llamado **''informática''** se refiere al conjunto de conocimientos científicos y técnicas que hacen posible el tratamiento automático de la información por medio de computadoras. En otras palabras, la informática es la ciencia que estudia el manejo y administración de la información.

La informática se aplica a numerosas y variadas áreas del conocimiento, como por ejemplo, en gestión de negocios, almacenamiento y consulta de información, monitorización y control de procesos, industria, robótica, comunicaciones, control de transportes, investigación, desarrollo de juegos, diseño computarizado, aplicaciones o herramientas multimedia, medicina, biología, física, química, meteorología, ingeniería, arte, entre otras. Actualmente es difícil concebir un área que no utilice, de alguna manera, el apoyo de la informática. 

Entre las funciones principales de la informática se pueden enunciar las siguientes:

+ Creación de nuevas especificaciones de trabajo
+ Desarrollo e implementación de sistemas informáticos
+ Sistematización de procesos
+ Optimización de los métodos y sistemas informáticos existentes
+ Facilitar la automatización de datos


**¿Qué es una computadora?**
====================================

Una computadora, también conocida como PC (por sus siglas en inglés, Personal Computer) es una máquina electrónica creada para recibir y procesar datos con el fin de convertirlos en información conveniente y útil.

Está constituida por dos partes esenciales: el **hardware**, que es su composición física (circuitos electrónicos, cables, gabinete, teclado, etcétera) y su **software**, siendo ésta la parte intangible (programas, datos, información, etcétera).


**Hardware**
===================

Como ya se ha mencionado, la parte hardware se refiere a todo lo físico, lo tangible, los componentes eléctricos, mecánicos, electromecánicos y electrónicos, tales como cables y elementos externos que permiten la comunicación entre el usuario y la computadora.

El hardware está compuesto por los siguientes elementos:

#. **Unidad Central de Procesamiento (UCP)**

#. **Memoria Central o Principal**

#. **Dispositivos Periféricos**

#. **Buses**


1. Unidad Central de Procesamiento (UCP)
----------------------------------------

La Unidad Central de Procesamiento, también conocida como CPU (por sus siglas en inglés, *Central Processing Unit*), se puede considerar como el cerebro de la computadora. Su función es controlar todas las operaciones efectuadas por la computadora y comunicarse con las demás partes del sistema.

Esta Unidad se encuentra en un circuito integrado (o chip) que recibe el nombre de **microprocesador**. A su vez, éste se encuentra junto con otros chips y dispositivos electrónicos, en un tablero que actúa como soporte denominado tarjeta o **placa madre** (en inglés, *motherboard*).

Las operaciones que realiza se pueden resumir en:

- Lectura de información de un dispositivo de entrada.

- Procesamiento de datos de entrada que incluye: 1) operaciones aritméticas y 2) comprobaciones y operaciones lógicas.

- Escritura de información utilizando un dispositivo de salida.


La UCP está compuesta por los siguientes componentes básicos: la **Unidad de Control**, la **Unidad Aritmética y Lógica** y un banco de **Registros**. A continuación, se esquematiza la arquitectura mencionada:


.. figure:: images/u0_DiagramaVonNeumann.png
    :align: center
    :width: 1133px
    :height: 967px
..    :scale: 100 %


1.1 Unidad de Control (UC)
...........................

Esta unidad efectúa la administración de los demás componentes de la computadora y realiza el seguimiento de las tareas encomendadas, enviando señales de control hacia los distintos dispositivos para posibilitar la ejecución de las instrucciones leídas.

Sus funciones básicas son:

- Captar las instrucciones del programa almacenado en la memoria principal.

- Decodificar o interpretar las instrucciones.

- Ejecutar las instrucciones, asignando la tarea al dispositivo responsable.

- Administrar mediante buses (o cables), la comunicación y la circulación de los datos.

1.2 Unidad Aritmética y Lógica (UAL)
.....................................

Es la encargada de efectuar las operaciones elementales de tipo aritmético (sumas, restas, productos y divisiones) y de tipo lógico (comparaciones). A través de un bus interno se comunica con la UC, la cual le envía los datos y le indica la operación a realizar.

1.3 Banco de Registros
.........................

Este banco proporciona un espacio de almacenamiento para los datos con los que trabaja la UCP. Los registros se deben cargar con información que proviene de la memoria principal antes de comenzar a operar. Cuando se necesita operar con nuevos datos, la información en el banco de registros se debe liberar con lo cual su valor debe escribirse en la memoria principal.

Operar con datos en el banco de registros es mucho más rápido que operar con datos que se encuentran en la memoria principal. Por esta razón, cuanto mayor sea el banco de registros se requerirán menos intercambios con la memoria principal y la tarea se realizará antes.



2. Memoria Central o Principal
-------------------------------

La memoria principal tiene por objeto almacenar la información que es accesible a la UCP. Aquí se almacenan las instrucciones que serán ejecutadas por la UCP y también los datos que serán procesados por la misma unidad.

Por otra parte, la UCP puede leer y/o escribir datos en las diferentes posiciones de memoria que componen la memoria principal.

La memoria principal de una computadora consta de memorias RAM y memorias ROM en cantidad y proporción variable:

**Memoria RAM** (por sus siglas en inglés, *Random Access Memory*): es una memoria de acceso aleatorio que permite la lectura/escritura. Se utiliza para almacenar los programas necesarios para el funcionamiento de la computadora. 

Las instrucciones que reciba la computadora y la información que ésta procese serán guardadas en la RAM por el tiempo que dure una sesión de trabajo. La memoria RAM de la computadora no es un lugar de almacenamiento permanente de información; sólo se encontrará activa mientras la computadora esté encendida.

**Memoria ROM**  (por sus siglas en inglés, *Read-Only Memory*): es una memoria de sólo lectura. Aquí se almacenan ciertos procedimientos e información que necesita la computadora al encenderse. Estas instrucciones están grabadas y permanecen inalterables en el chip de memoria ROM, por lo que no pueden ser modificadas por el usuario.

Esta memoria además es no volátil, es decir, que su contenido no se modifica ni se destruye en caso de que se apague la computadora.


3. Dispositivos Periféricos
----------------------------

Son aquellos dispositivos auxiliares e independientes que se conectan a la UC de una computadora. Se consideran periféricos tanto a los dispositivos a través de los cuales la computadora se comunica con el mundo exterior, como a los sistemas que almacenan información, sirviendo de memoria auxiliar de la memoria principal.

En otras palabras, se entiende por periférico al conjunto de dispositivos que, sin pertenecer al núcleo fundamental de la computadora (formado por la UCP y la memoria central), permiten realizar operaciones de entrada y/o salida complementarias al procesamiento de datos que realiza la UCP.

Se pueden clasificar en: **periféricos de entrada**, **periféricos de salida** y **periféricos de entrada/salida** (de almacenamiento).   


3.1 Periféricos de Entrada
............................

Permiten el ingreso de datos de diferente tipo como sonido, imagen, texto, entre otros. A continuación, se mencionan algunos ejemplos de este tipo de periférico:

**Teclado:** Es un dispositivo que sirve para introducir datos de texto, números y combinaciones de teclas que se convierten en comandos. Luego éste envía los datos ingresados al microprocesador para que sean interpretados.

.. figure:: images/u0_teclado.jpg
    :align: center
    :scale: 30 %

**Mouse:** Es un dispositivo electrónico que se conecta a la computadora, que consta de uno a tres botones convenientemente dispuestos para ser manejados con los dedos, y que sirve como dispositivo auxiliar de entrada de datos, facilitando la comunicación con los programas que se ejecuten.

El Mouse se puede mover sobre una superficie lisa de tal modo que sus desplazamientos sirven para arrastrar y mover el puntero sobre la pantalla. La posición de éste se cambia moviendo el Mouse sobre una mesa o sobre la superficie del escritorio. También se podrá utilizar para seleccionar un elemento; sólo se debe señalar el elemento para luego pulsar el botón izquierdo. Al proceso descrito anteriormente, se lo conoce como click.

.. figure:: images/u0_mouse.png
    :align: center
    

**Escáneres:** Son periféricos diseñados para registrar caracteres escritos o gráficos en forma de fotografías o dibujos, impresos en una hoja de papel, facilitando su introducción a la computadora convirtiéndolos en información binaria comprensible para ésta. 

El funcionamiento de un escáner es similar al de una fotocopiadora. Se coloca una hoja de papel que contiene una imagen sobre una superficie de cristal transparente. Bajo el cristal existe una lente especial que realiza un barrido de la imagen existente en el papel; al realizar el barrido, la información existente en la hoja de papel es convertida en una sucesión de información en forma de unos y ceros que se introducen en la computadora.

.. figure:: images/u0_escaner.jpg
    :align: center


Otros periféricos de entrada: **micrófono, lápiz óptico, joystick, palancas y pedales de comando para juegos, cámaras web, lectores de códigos de barra, entre otros.**

3.2 Periféricos de Salida
...........................

Muestran información al usuario después de que ésta haya sido procesada, ya sea en forma de imagen, texto, sonido u otro tipo. Entre los periféricos de salida más utilizados se pueden encontrar:

**Monitor:** Es la principal salida visual de la computadora. Es una pantalla que requiere de una tarjeta de vídeo encargada de enviar las señales para formar las imágenes, las que están constituidas por puntos (los píxeles), dibujados a partir de dichas señales.

.. figure:: images/u0_monitor.jpg
    :align: center
    :scale: 70 %

**Impresora:** Es el dispositivo que permite plasmar información en un medio físico como el papel. Dependiendo de la tecnología empleada, las impresoras pueden clasificarse en:

*Impresoras de impacto:* son aquellas en las que existe un dispositivo mecánico impresor que golpea al papel para imprimir el carácter (parecido a la forma de imprimir de una máquina de escribir). Ejemplos de este tipo son las impresoras matriciales.

*Impresoras de no impacto:* chorro de tinta y láser.

También se deben mencionar a los **plotters**, que son impresoras especiales utilizadas en general para dibujo técnico o gráficos de alta calidad. Emplean plumas con diferentes colores y el papel de impresión puede avanzar y retroceder para facilitar la tarea. Son empleadas en diseño gráfico, arquitectura e ingeniería.


.. figure:: images/u0_matricial.jpg  
    :align: center

    Impresora Matricial

.. figure:: images/u0_laser.jpg
    :align: center
    :scale: 15 %
    
    Impresora Láser

.. figure:: images/u0_tinta.jpg
    :align: center
    :scale: 50 %
    
    Impresora Chorro o Inyección de Tinta
    
Otros periféricos de salida: **auriculares, parlantes, proyectores, entre otros.**

3.3 Periféricos de Entrada/Salida
...................................

Son aquellos que permiten el ingreso de datos a la computadora como así también la salida de la información procesada.
Se pueden nombrar las **lectograbadoras de CD/DVD**, las **impresoras multifunción**, los **módems** y las **interfaces de red**, entre otros.

Dentro de este categoría se destacan los dispositivos de almacenamiento:

Dispositivos de Almacenamiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son aquellos que se utilizan para almacenar los datos y programas. Dependiendo de la tecnología que utilizan, existen distintos dispositivos de almacenamiento, los que se describen a continuación:

a) Dispositivos de Almacenamiento Magnéticos
_____________________________________________


Son aquellos que utilizan las propiedades magnéticas de ciertos materiales para almacenar información digital.

**Disquete:** Es un dispositivo que alcanza una capacidad de almacenamiento de información de hasta 1,44 MB. Su uso en la actualidad es escaso o nulo, puesto que se ha vuelto obsoleto teniendo en cuenta los avances que en materia de tecnología se han producido.

**Disco rígido o disco duro:** Es el dispositivo que tiene la capacidad de guardar grandes cantidades de información de forma permanente. Está formado por varios discos metálicos apilados sobre los que se mueve una pequeña cabeza magnética que graba y lee la información.

El disco duro almacena casi toda la información que se maneja al trabajar con una computadora: el sistema operativo, los programas, archivos de texto, imagen, vídeo y otros.

Los discos rígidos se caracterizan por:

- Gran capacidad de almacenamiento, que puede variar entre 120 GB y 4 TB.
- Tiempo de acceso muy rápido.
- Alta velocidad de transferencia de información.
- Seguridad.

Pueden ser internos (fijos) o externos (portátiles) dependiendo del lugar que ocupen en el gabinete de la computadora.

.. figure:: images/u0_disco.png
    :align: center
    :scale: 90 %

b) Dispositivos de Almacenamiento Ópticos
_____________________________________________

Son aquellos que utilizan una luz láser como parte del proceso de lectura o escritura de datos. La información se graba de adentro hacia afuera en forma de espiral por medio del láser. Por otra parte, la información queda grabada en la superficie de manera física, por lo que sólo el calor y las ralladuras pueden producir la pérdida de los datos, pero en cambio es inmune a los campos magnéticos y la humedad.

**CD:** (por sus siglas en inglés *Compact Disc*) posee una capacidad de almacenamiento que puede alcanzar los 700 MB.

**DVD:** (por sus siglas en inglés *Digital Versatile Disc*) se asemeja al CD, pero la gran diferencia radica en que su capacidad de almacenamiento es mayor, llegando hasta los 4.7 GB. 

**Blu-ray:** también conocido como BD, es un formato de disco óptico de nueva generación de 12 cm de diámetro para video de alta definición y almacenamiento de datos de alta densidad. Es 5 veces mejor que el DVD. Su capacidad de almacenamiento llega a 25 GB por capa.

c) Dispositivos de Almacenamiento de Estado Sólido
___________________________________________________

Los **dispositivos de estado sólido o SSD** (por sus siglas en inglés, *Solid-State Drive*) utilizan memoria no volátil para almacenar datos, en lugar de los platos o discos magnéticos de las unidades de discos duros convencionales.

**Discos duros SSD:** En comparación con los discos duros tradicionales, las unidades de estado sólido son menos sensibles a los golpes al no tener partes móviles, son prácticamente inaudibles, y poseen un menor tiempo de acceso y de latencia, lo que se traduce en una mejora del rendimiento exponencial en los tiempos de carga de los sistemas operativos. En contrapartida, su vida útil es muy inferior, ya que tienen un número limitado de ciclos de escritura, pudiendo producirse la pérdida absoluta de los datos de forma inesperada e irrecuperable. 

.. figure:: images/u0_discoSSD.jpg
    :align: center
    :scale: 65 %
    
**Memoria USB o Pendrive:** no es más que una unidad de almacenamiento de datos con una memoria de estado sólido que se conecta a la PC a través de un puerto USB. Tienen una capacidad de almacenamiento que va desde entre los 2 GB hasta los 512 GB. En la actualidad existen pendrives de 1 TB y 2 TB, aunque su comercialización es incipiente. Su principal utilidad es la portabilidad: permite transportar gran cantidad de información entre distintos lugares dado su pequeño tamaño. Es un medio que no falla y es seguro. 

.. figure:: images/u0_usb.jpg
    :align: center
    :scale: 25 %
    

    
4. Buses
-------------

Son los cables o pistas que enlazan todos los elementos internos de la computadora. Transfieren datos entre los componentes de una computadora o entre varias computadoras.
    
    
**Software**
===================

Es una de las partes fundamentales de una computadora, tal como lo es el hardware. El software es el conjunto de instrucciones que las computadoras necesitan para manipular datos. Sin software la computadora sería un conjunto de partes físicas que no se podrían utilizar.

El hardware por sí solo no puede hacer nada, es necesario que exista el software, que le proporciona al microprocesador las instrucciones individuales para procesar los datos y generar los resultados esperados.

Según el uso para el cual fue creado, el Software se clasifica en:

- Sistema Operativo o Software del Sistema.
- Software de aplicación.


Sistema Operativo
------------------

Es el software principal en toda computadora porque es el que sirve como base para los demás programas instalados. El sistema operativo es el que se encarga de reconocer las conexiones e interacciones entre los dispositivos físicos y los programas, además de proporcionar una “comunicación amigable” con el usuario.

En unidades posteriores, se tratará con más detalles el concepto de sistemas operativos.

Software de Aplicación
-----------------------

Comprende una amplia gama de programas que permiten escribir, hacer cálculos, gráficos, escuchar música, comunicarse y muchas más actividades. Están diseñados para facilitar al usuario la realización de una tarea específica.

En la *Unidad 1: Introducción a GNU/Linux*, se verán las distintas aplicaciones que vienen instaladas por defecto en un sistema GNU/Linux y cuáles son las tareas que permiten realizar.



**Bibliografía**
===================

* *''Introducción a la Informática''*. Carlos Brys.  Universidad Nacional de Misiones. 2014. Online: http://docplayer.es/3461474-Introduccion-a-la-informatica.html
* *''Aprendamos a manejar el computador''*. Convenio Universidad del Cauca - Computadores para Educar para la Región Sur Pacífico. Facultad de Ingeniería Electrónica y Telecomunicaciones - FIET, Departamento de Sistemas. Agosto de 2009.
* *''La Computadora - Conceptos Básicos''*. Cátedra ''Informática (Electiva)''. Facultad de Ingeniería y Ciencias Hídricas de la Universidad Nacional del Litoral.
* *Apuntes del curso ''Sistemas operativos monousuario y multiusuario''*. Instituto Puig Castellar de Santa Coloma de Gramanet, Barcelona, España.

