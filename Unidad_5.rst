================================================
Software Libre: Ofimática en las Organizaciones 
================================================
------------------------------------------------
Unidad 5: Entornos Gráficos de Escritorio
------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Unidad 5 - Entornos Gráficos de Escritorio

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022

:Autor: Leopoldo Bertinetti


.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak 

.. contents:: Contenido

.. raw:: pdf

    PageBreak


    
Módulo 1: Introducción
==============================================

En esta unidad se abordará de manera sencilla y simplista el concepto de entornos gráficos de escritorio, dejando a criterio del alumno profundizar en la temática. Se explicarán 4 componentes de este complejo sistema, que trabajan de manera independiente para cumplir el objetivo de mostrar los gráficos del sistema. Ellos son: el *Servidor X*, el *Gestor de Pantalla*, el *Gestor de Ventanas* y el *Entorno de Escritorio*.

A continuación, se muestra un esquema de las capas de la interfaz gráfica de usuario con los elementos mencionados:

.. figure:: images/u5_capas.png
    :align: center
    :scale: 250 %

1.1 X Server (Servidor X)
----------------------------------------------------------

El Servidor X, también llamado *sistema de ventanas X* (en inglés, X Window System), es un estándar para proporcionar capacidades gráficas a un sistema operativo. En otras palabras, es la base que permite dibujar elementos gráficos en la pantalla. Al mismo tiempo, permite la interacción gráfica en red entre un usuario y una o más computadoras haciendo transparente la red para éste.

Este sistema fue desarrollado por el MIT en los años 80. Es usado mayoritariamente en Unix y sistemas estilo Unix tales como Linux.

En otras palabras, el servidor X es el encargado de mostrar la información gráfica en la pantalla de forma totalmente independiente al sistema operativo.

También se suele utilizar el término *X11* para referirse a él, que es la especificación del protocolo.

Actualmente existe una alternativa a este protocolo, **Wayland** [#]_, que propone un enfoque más simple y más eficiente, aunque también posee sus propios problemas.​

.. [#] Wayland (protocolo): <https://es.wikipedia.org/wiki/Wayland_(protocolo)>

1.2 Display Manager (Gestor de Pantalla)
----------------------------------------------------------

Ejecutar únicamente el servidor X sólo lleva a una pantalla vacía. Por dicha razón, se necesita gestionar las sesiones de usuarios. Dicha tarea la realiza el gestor de pantalla [#]_, también llamado *gestor de inicio de sesión*, un componente de software que muestra una pantalla para la autenticación de usuarios e inicia el escritorio gráfico una vez que se autenticó el usuario.


.. [#] Display Manager: <https://wiki.archlinux.org/title/Display_manager_(Espa%C3%B1ol)>

El gestor de pantalla predeterminado de X es **XDM** (*X Display Manager*). Sin embargo, la mayoría de las distribuciones de Linux utilizan otros gestores más potentes, tales como **GDM** (*GNOME Display Manager*), **KDM** (*KDE Display Manager*) y **LightDM** (*Light Display Manager*). Éstos permiten, además de la conexión y cierre de sesiones, el cambio de idioma para una sesión, la selección del gestor de ventanas e incluso del entorno de escritorio antes de iniciar una sesión.

El gestor de pantalla por defecto en el entorno KDE era KDM, pero en las últimas versiones fue reemplazado por **SDDM** (*Simple Desktop Display Manager*).

.. figure:: images/u5_kdm.png
    :align: center
    :scale: 50 %
    
    Gestor KDM

.. figure:: images/u5_sddm.png
    :align: center
    :scale: 72 %
    
    Gestor SDDM
    
Es importante tener en cuenta que GDM pasó por un serio rediseño en Gnome 3 / Gnome Shell, por lo que la nueva versión de GDM (generalmente denominada **GDM3**) es bastante diferente a la anterior.

.. figure:: images/u5_gdm3.png
    :align: center
    :scale: 55 % 
    
    Gestor GDM3
    
Debian Xfce utiliza el gestor LightDM, al igual que Xubuntu.

.. figure:: images/u5_lightdm_debian.png
    :align: center
    :scale: 55 % 
    
    Gestor LightDM


1.3 Windows Manager (Gestor de Ventanas)
-------------------------------------------

Un gestor de ventanas es el software encargado de mostrar las ventanas en los sistemas con interfaz gráfica. Además, controla la ubicación, apariencia y tamaño de las ventanas. Por lo tanto, las acciones asociadas al gestor de ventanas suelen ser: abrir, cerrar, minimizar, maximizar, mover, escalar y mantener un listado de las ventanas abiertas. También controla la barra de títulos y determina la ventana activa (comportamiento del foco), recibiendo los comandos del mouse y teclado para ella. 

Un gestor de ventanas es, desde el punto de vista del servidor X, una aplicación cliente normal y, por lo tanto, se comunica con el servidor X como cualquier otra aplicación.

Puede formar parte del entorno de escritorio o ser independiente. Cuando se habla de un gestor de ventanas independiente se está refiriendo a un gestor que no requiere otra capa de software para poder ser funcional.

Los gestores de ventanas [#]_ difieren entre sí de muchas maneras, incluyendo apariencia, consumo de memoria, opciones de personalización, escritorios múltiples o virtuales y similitud con ciertos entornos de escritorio ya existentes, entre otras. Se pueden mencionar nombres como **Metacity**, **Mutter** (ambos empleados en GNOME), **KWin** (para KDE), **Compiz**, **XFWM** (XFCE), **Enlightenment**, **Blackbox**, **IceWM**, **Fluxbox**, **Openbox**, entre otros.

.. [#] Window Manager: <https://wiki.archlinux.org/title/Window_manager>

1.4 Desktop Environment (Entorno de Escritorio)
----------------------------------------------------------

Un entorno de escritorio (EE) es un conjunto de software que ofrece al usuario de una computadora una interacción amigable y cómoda. Es una implementación de interfaz gráfica de usuario que ofrece facilidades de acceso y configuración, como barras de herramientas e integración entre aplicaciones con posibilidad de arrastrar y soltar. 

Un EE consta, al menos, de las siguientes aplicaciones: un gestor de ventanas, un administrador de vistas, una barra de tareas, un gestor de archivos y un conjunto de aplicaciones. Se puede decir que un EE está compuesto por varios componentes empaquetados por separado que, en conjunto, proporcionan la funcionalidad completa del entorno. Sin embargo, se pueden seleccionar por separado para que el usuario pueda adaptar el ambiente de trabajo a sus necesidades.

Los EE por lo general no permiten el acceso a todas las características que se encuentran en un sistema operativo. Por dicha razón, la tradicional interfaz de línea de comandos todavía se utiliza cuando se requiere el control total sobre el sistema operativo. 



Módulo 2: Entornos de Escritorio
============================================================

GNU/Linux ofrece gran variedad de entornos y nos brinda la libertad de poder elegir el que nos parezca más adecuado a nuestras necesidades y gustos, pudiendo obtener un mix de aplicaciones y funcionalidades.

A continuación, se mencionarán algunos de los más conocidos:


2.1 XFCE
----------------------------------------------------------

XFCE es un entorno de escritorio ligero para sistemas tipo UNIX. Su objetivo es ser rápido y usar pocos recursos del sistema, sin dejar de ser visualmente atractivo y fácil de usar.

Se compone de una serie de aplicaciones que proporcionan toda la funcionalidad que se puede esperar de un moderno entorno de escritorio. Se empaquetan por separado y puede escogerse entre los paquetes disponibles para crear un entorno óptimo personal para el trabajo.

**Lecturas Recomendadas:**  https://xfce.org/about - https://xfce.org/about/tour

.. figure:: images/u5_xfce.png
    :align: center
    :scale: 55 % 
    
    Entorno XFCE en Xubuntu (XFCE + Ubuntu)

2.2 KDE
----------------------------------------------------------

KDE comenzó su vida como un entorno de escritorio hace más de 20 años. A medida que sus actividades fueron creciendo, KDE se convirtió en un equipo internacional que desarrolla software libre y de código abierto. Este equipo ofrece un entorno gráfico avanzado, una amplia variedad de aplicaciones para la comunicación, el trabajo, la educación y el entretenimiento y una plataforma para crear fácilmente nuevas aplicaciones. Incluso en la actualidad mantienen una distribución propia denominada *KDE neon*.

Hablando exclusivamente del entorno, posee las siguientes características:

* Un escritorio que intenta ser moderno y atractivo.
* Un sistema flexible y configurable, que permite personalizar aplicaciones sin necesidad de editar archivos de texto.
* La transparencia de la red permite acceder fácilmente a archivos de otras redes y computadoras.
* Un ecosistema de software con cientos, incluso miles de programas.
* Disponible en más de 60 idiomas.

Se suele referir a este entorno con el nombre *KDE Plasma* (KDE versión 5).

**Lecturas Recomendadas:**  https://kde.org/es/plasma-desktop/ - https://userbase.kde.org/Plasma/es - https://userbase.kde.org/Applications/es

.. figure:: images/u5_kde.png
    :align: center
    :scale: 55 % 
    
    Entorno KDE en Kubuntu (KDE + Ubuntu)

    
2.3 GNOME
----------------------------------------------------------

GNOME es un entorno de escritorio e infraestructura de desarrollo para sistemas operativos GNU/Linux, Unix y derivados, compuesto enteramente de software libre. GNOME es dirigido por la organización sin fines de lucro llamada *Fundación GNOME*. Agrupa empresas, voluntarios, profesionales y organizaciones no lucrativas de todo el mundo.

El proyecto GNOME es una parte importante del ecosistema del software libre. Muchos de los que trabajan para el proyecto GNOME, también lo hacen en otros proyectos de software libre.

El entorno GNOME provee un gestor de ventanas "intuitivo y atractivo" y una plataforma de desarrollo para crear aplicaciones que se integran con el escritorio. El Proyecto pone énfasis en la simplicidad, facilidad de uso y eficiencia. Tiene como objetivo la libertad para crear un entorno de escritorio que siempre tendrá el código fuente disponible para reutilizarse bajo una licencia de software libre.

**Lecturas Recomendadas:**  https://www.gnome.org/ - https://help.gnome.org/ - https://release.gnome.org/ - https://foundation.gnome.org/

.. figure:: images/u5_gnome.png
    :align: center
    :scale: 55 % 
    
    Entorno GNOME en Ubuntu

2.4 LXDE
----------------------------------------------------------

LXDE, siglas en inglés de *Lightweight X11 Desktop Environment*, que en español significa *Entorno de Escritorio X11 Liviano*, es un entorno de escritorio extremadamente rápido, ligero y ahorrador de energía. Trae una interfaz agradable al usuario, soporte multi-idioma , atajos de teclado estándar, características adicionales como navegación por pestañas y es mantenido por una comunidad internacional de desarrolladores. 

LXDE es un proyecto que apunta a entregar un entorno de escritorio ligero y rápido. No está diseñado para ser tan complejo como KDE o GNOME, pero es bastante usable y ligero, y mantiene una baja utilización de recursos y energía. Especialmente diseñado para computadoras con especificaciones de hardware limitadas.

Al contrario de otros escritorios fuertemente integrados, LXDE se esfuerza en ser modular, de forma que cada componente puede usarse independientemente con pocas dependencias. Esto hace que portar LXDE a distintas distribuciones y plataformas sea más fácil.

Una distribución que utilizaba este entorno era *Lubuntu* (LXDE + Ubuntu), pero a partir de la versión 18.10 comenzó a utilizar por defecto el entorno **LXQt**, entorno que surgió por la fusión de otros dos proyectos: LXDE y Razor-qt [#]_. 

.. [#] Razor-qt: https://es.wikipedia.org/wiki/Razor-qt

**Lecturas Recomendadas:** http://www.lxde.org/ - https://lxqt-project.org/

.. figure:: images/u5_lubuntu.png
    :align: center
    :scale: 55 % 
    
    Entorno LXDE en Xubuntu

.. figure:: images/u5_lubuntu_lxqt.png
    :align: center
    :scale: 21 % 
    
    Entorno LXQt en Lubuntu 22.04
        
    
2.5 UNITY
----------------------------------------------------------

Unity es una interfaz de usuario creada para la distribución Ubuntu. El proyecto fue iniciado en 2010 por Mark Shuttleworth, fundador de Canonical Limited (creadora de Ubuntu). Este entorno fue diseñado con el propósito de aprovechar el espacio en las pantallas pequeñas de las netbooks, especialmente el espacio vertical.

Su primer lanzamiento se realizó con la versión 10.10 de Ubuntu Netbook Remix. 

Su interfaz está compuesta de tres importantes elementos: *Lanzador* (ubicado al lado izquierdo de la pantalla, se utiliza para albergar accesos directos a las aplicaciones que se deseen, y también como función de lista de ventanas), *Tablero de aplicaciones* (despliega todos los accesos a aplicaciones, archivos, música y videos del usuario) y *Barra de menús* (ubicada en la parte superior de la pantalla, se utiliza para desplegar los menús e indicadores).

En la versión 18.04 de Ubuntu, el escritorio Unity fue reemplazado por GNOME. Sin embargo, aún sigue disponible para instalarlo. 

**Lecturas Recomendadas:**  https://web.archive.org/web/20161030031204/http://unity.ubuntu.com/ 

.. figure:: images/u5_unity.png
    :align: center
    :scale: 55 % 
    
    Entorno UNITY en Ubuntu

2.6 Otros
---------------------------------------------------------

Si desean seguir profundizando en este tema, se mencionan otros entornos de escritorio para que investiguen sus particularidades y características: *Cinnamon*, *Mate*, *Pantheon*, *Deepin Desktop Environment*.


.. raw:: pdf

    PageBreak


**Actividad Propuesta** (no es obligatoria)
================================================================

Para probar diferentes entornos gráficos, se propone la siguiente actividad:

En la máquina virtual (VM) donde se instaló Debian XFCE, instalar los siguientes entornos de escritorio: GNOME, KDE, LXDE. También el gestor de ventanas IceWM.
    
**IMPORTANTE:**

*  Verificar que el disco duro virtual tenga al menos 20 GB de espacio libre.

* Tener en cuenta que la instalación de distintos entornos puede ocasionar una inestabilidad del sistema. Por dicha razón, se recomienda que se instalen en la máquina virtual (VM).

* Se sugiere que se clone la VM, para que en el caso de que ocurra algún error, se tenga una copia de la VM. Así se evita que se deba instalar nuevamente.

* Antes de instalar cada paquete, comprobar si se posee el espacio de disco que requiera.

**Procedimiento:**

1) Instalar las actualizaciones del sistema con Synaptic (tal como se vió en la Unidad 2). Reiniciar en el caso de que se requiera.

2) Abrir el gestor de paquetes Synaptic.

3) Buscar el paquete correspondiente al entorno. En Debian los paquetes son los siguientes:

* **GNOME:** gnome
* **KDE:** kde-standard
* **LXDE:** lxde

.. figure:: images/u5_synaptic_gnome.png
    :align: center
    :scale: 55 % 

    Paquete gnome en Synaptic

.. figure:: images/u5_synaptic_kde.png
    :align: center
    :scale: 55 % 

    Paquete kde-standard en Synaptic    
    
.. figure:: images/u5_synaptic_lxde.png
    :align: center
    :scale: 55 % 

    Paquete lxde en Synaptic
    
4) Marcar para instalar y luego aplicar cambios. Durante la instalación es posible que se nos solicite seleccionar el gestor de pantalla de nuestra preferencia: dejar a *LightDM* como gestor por defecto. **Nota:** se recomienda instalar un entorno por vez.  

.. figure:: images/u5_actividad1.png
    :align: center
    :scale: 55 % 
    
.. figure:: images/u5_actividad2.png
    :align: center
    :scale: 55 % 
    
5) Una vez instalado el paquete, reiniciar. 

6) En el gestor de pantalla, se podrá seleccionar con qué entorno ingresar a la sesión del usuario.

.. figure:: images/u5_actividad3.png
    :align: center
    :scale: 55 % 

    Al seleccionar la opción LXDE se podrá ingresar a ese entorno

.. figure:: images/u5_actividad4.png
    :align: center
    :scale: 55 % 

    Entorno LXDE en Debian
    
7) Repetir los pasos para instalar los dos entornos restantes. 

.. figure:: images/u5_actividad5.png
    :align: center
    :scale: 55 % 
    
    Entorno GNOME en Debian
    
.. figure:: images/u5_actividad6.png
    :align: center
    :scale: 55 % 
    
    Entorno KDE (Plasma) en Debian 
    
8) Buscar el paquete "IceWM" en Synaptic e instalarlo. Reiniciar.

9) En la pantalla de bienvenida, seleccionar "IceWM Session".
    
.. figure:: images/u5_actividad7.png
    :align: center
    :scale: 55 % 
    
    Gestor de ventanas IceWM
    
Al final, desde el Gestor de Pantalla podremos seleccionar entre los distintos entornos que hemos instalado:

.. figure:: images/u5_actividad8.png
    :align: center
    :scale: 55 % 
    

.. raw:: pdf

    PageBreak


**Bibliografía**
================================================================

*Configuración del servidor X11*. Online: `https://debian-handbook.info/browse/es-ES/stable/ workstation.html#sect.x11-server-configuration <https://debian-handbook.info/browse/es-ES/stable/workstation.html#sect.x11-server-configuration>`_

*X.Org: sistema X Window*. Online: `http://recursostic.educacion.es/observatorio/web/fr/software/ software-general/715-xorg-sistema-x-window <http://recursostic.educacion.es/observatorio/web/fr/software/software-general/715-xorg-sistema-x-window>`_

*Wayland (protocolo)*. Online: `https://es.wikipedia.org/wiki/Wayland_(protocolo) <https://es.wikipedia.org/wiki/Wayland_(protocolo)>`_

*Wayland*. Online: `https://wayland.freedesktop.org/ <https://wayland.freedesktop.org/>`_

*Entornos de Escritorio*. Online: `https://debian-handbook.info/browse/es-ES/stable/sect.graphical-desktops.html <https://debian-handbook.info/browse/es-ES/stable/sect.graphical-desktops.html>`_

*GNOME*. Online: `https://es.wikipedia.org/wiki/GNOME <https://es.wikipedia.org/wiki/GNOME>`_
