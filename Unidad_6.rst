================================================
Software Libre: Ofimática en las Organizaciones 
================================================
--------------------------------------------------------------------
Unidad 6: Modificación y Personalización de Aplicaciones, Plugins
--------------------------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Unidad 6 - Modificación y Personalización de Aplicaciones, Plugins

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022

:Autor: Leopoldo Bertinetti

.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

.. contents:: Contenido

.. raw:: pdf

    PageBreak


    
Módulo 1: Requerimientos Previos
==================================

Antes de comenzar, se debe verificar que estén instaladas las siguientes aplicaciones:

* Mozilla Firefox. Página web: https://www.mozilla.org/es-AR/firefox/new/
* Mozilla Thunderbird. Página web: https://www.thunderbird.net/es-AR/ 
* LibreOffice. Página web: http://es.libreoffice.org/ 
* VLC. Página web: https://www.videolan.org/vlc/ 

Si alguna de las aplicaciones no está instalada, se debe instalar a través de Synaptic como se vió en unidades anteriores.



Módulo 2: Modificación y Personalización de Aplicaciones, Plugins
==================================================================

Así como se ha visto la personalización de ciertos aspectos del sistema, también se podrán personalizar algunas aplicaciones como por ejemplo: Firefox, LibreOffice, Thunderbird y VLC.

Al personalizar estas aplicaciones que se utilizan frecuentemente, se puede facilitar y agilizar su uso en el trabajo diario frente a la computadora. Esto se realiza mediante ciertos componentes de software llamados *"plugins"*.

Un **plugin** es una aplicación que añade una funcionalidad adicional o una nueva característica al software. Por lo tanto, se puede nombrar al plugin como un complemento.

Lo habitual es que el plugin sea ejecutado y añadido mediante el software principal, con el que interactúa a través de una cierta interfaz. Actualmente, la mayoría de los programas trabajan con plugins.

Se utilizan como sinónimos de plugin, dependiendo del software, a los siguientes conceptos: *complementos*, *addons*, *extensiones*, entre otros.


2.1 Goodies (Plugins para Xfce)
--------------------------------

Como se ha visto, Debian Xfce ya viene con muchas aplicaciones instaladas para utilizar en nuestro trabajo diario.

Sin embargo, con el trabajo cotidiano nos daremos cuenta que hay ocasiones en las que usamos continuamente ciertas aplicaciones y nos sería útil conocer cierta información actual de la computadora o de otras aplicaciones. Para facilitar el acceso a nuestras aplicaciones favoritas u obtener información instantáneamente de la computadora, podemos utilizar plugins para el escritorio de trabajo que estemos utilizando.

En el entorno Xfce estos plugins particularmente se llaman *"Goodies"* y permiten potenciar y adaptar el sistema a nuestro gusto.

En la página de "Xfce Goodies" (http://goodies.xfce.org/) se pueden explorar los distintos plugins que se encuentran disponibles.

.. figure:: images/u6_goodies1.png
    :align: center
    :scale: 55 % 

    Página principal Xfce Goodies

Para utilizar estas funcionalidades se debe instalar el paquete *xfce4-goodies* (desde el Synaptic, por ejemplo). De esta manera, se descargarán y guardarán varios complementos del escritorio, los cuales se podrán agregar y configurar.

Como podrán observar, estos complementos ya vienen instalados en Debian Xfce:

.. figure:: images/u6_goodies2.png
    :align: center
    :scale: 55 % 

A continuación, se verán algunos ejemplos:

2.1.1 Panel Plugins
...........................

Cuando se instala el paquete, se instalarán algunos plugins, tales como lanzadores o accesos directos, gráficas de utilización de CPU, memoria, entre otros; permitiendo tener una mayor funcionalidad del sistema. 

Para agregar estos complementos en el panel principal, se debe hacer clic derecho sobre el panel y seleccionar "Panel". Se desplegará otro menú, en el cual se deberá elegir "Añadir elementos nuevos". Mostrará una lista de complementos que se pueden agregar al Panel.

.. figure:: images/u6_goodies_panel1.png
    :align: center
    :scale: 55 % 
    
.. figure:: images/u6_goodies_panel2.png
    :align: center
    :scale: 55 % 

A modo de ejemplo, se añadirá la "Actualización del tiempo". Se podrá ver que se agrega a la derecha del panel. En una primera instancia no mostrará información porque se debe configurar la ubicación.

.. figure:: images/u6_goodies_panel3.png
    :align: center
    :scale: 55 % 

Para configurarla, se debe hacer clic con el botón derecho sobre el complemento y luego seleccionar "Propiedades".

.. figure:: images/u6_goodies_panel4.png
    :align: center
    :scale: 55 % 

La aplicación intentará detectar la ubicación automáticamente. Si no lo logra, se puede colocar nuestra ubicación. Una vez configurada, clic en "Cerrar".

.. figure:: images/u6_goodies_panel5.png
    :align: center
    :scale: 55 % 

Se podrá comprobar que luego de configurado, el complemento mostrará información relativa acerca del tiempo.

.. figure:: images/u6_goodies_panel6.png
    :align: center
    :scale: 55 % 


2.1.2 Thunar Plugins
...........................

Al igual que los plugins del panel, los plugins de Thunar (gestor de archivos) se instalan para añadir más funcionalidades. Éstos se activarán automáticamente luego de instalar el paquete *xfce4-goodies*. 

Un ejemplo de estas funcionalidades es la generación de miniaturas para formatos de archivo menos comunes. Otras funcionalidades que se pueden mencionar son: un complemento de etiquetas de medios que permite agregar características especiales a los archivos multimedia - es decir, que se pueden ver y modificar propiedades de estos tipos de archivos sin necesidad de abrir ninguna aplicación especial-, un complemento para renombrar múltiples archivos a la vez y otros más que figuran en https://docs.xfce.org/xfce/thunar/start#thunar_plugins.

.. figure:: images/u6_goodies_thunar1.png
    :align: center
    :scale: 55 % 
    
    Complemento para renombrar múltiples archivos


2.2 LibreOffice
----------------------

LibreOffice es un paquete de ofimática de código abierto desarrollado por *The Document Foundation* (https://www.documentfoundation.org/), el cual se creó como bifurcación de *OpenOffice.org* (https://www.openoffice.org/) en 2010.

Está compuesto por un procesador de texto (Writer), un editor de hojas de cálculo (Calc), un gestor de presentaciones (Impress), un gestor de bases de datos (Base), un editor de gráficos vectoriales (Draw) y un editor de fórmulas matemáticas (Math).


2.2.1 Extensiones de LibreOffice
...................................

Se pueden ampliar las funcionalidades de LibreOffice aún más con las extensiones disponibles para descargar desde el centro de extensiones de LibreOffice. Su página web es https://extensions.libreoffice.org/.

Las *extensiones* son complementos que se instalan como extras a la descarga estándar de LibreOffice, y añaden algún tipo de funcionalidad adicional a la suite, ya sea para una aplicación en particular (Writer, Calc, Impress, etc.), o para todas las aplicaciones.

Para instalar estos complementos, primero se deben descargar los archivos desde la página de extensiones y luego hacer doble clic en el administrador de archivos. El sistema detectará automáticamente que son complementos de LibreOffice y se anexarán al mismo. Al abrir dicho software estarán disponibles para utilizarlos.

A continuación, se muestra un ejemplo:

2.2.1.1 Spanish Spellchecker
++++++++++++++++++++++++++++++

Con esta extensión se podrá agregar un completo diccionario de español a LibreOffice. Estas extensiones pertenecen al proyecto colaborativo para el desarrollo de herramientas de asistencia a la escritura para la lengua española (https://github.com/sbosio/rla-es).

El primer paso es buscarla en el centro de extensiones:

.. figure:: images/u6_libreoffice1.png
    :align: center
    :scale: 55 % 

.. figure:: images/u6_libreoffice2.png
    :align: center
    :scale: 55 % 
    
Se busca la última versión de la extensión y se descarga.

.. figure:: images/u6_libreoffice3.png
    :align: center
    :scale: 55 % 

El formato del archivo descargado es OXT.

.. figure:: images/u6_libreoffice4.png
    :align: center
    :scale: 55 % 

Para instalarlo, se debe hacer doble clic sobre él. El sistema reconoce la extensión y pregunta si se desea instalar.

.. figure:: images/u6_libreoffice5.png
    :align: center
    :scale: 55 % 

Se deberá esperar unos segundos hasta que la instalación termina. En la notificación que aparece, hacer clic en "Cerrar".

.. figure:: images/u6_libreoffice6.png
    :align: center
    :scale: 55 % 

Para finalizar, se debe reiniciar LibreOffice y se podrá utilizar la extensión.

.. figure:: images/u6_libreoffice7.png
    :align: center
    :scale: 55 % 


2.2.2 Plantillas de LibreOffice
..................................

Las plantillas ofrecen una manera de evitar la repetición de acciones al crear nuevos documentos de texto, hojas de cálculo o presentaciones. También ofrecen una manera de mantener la coherencia del diseño del documento y el contenido estándar.

Las plantillas (o *templates*) son archivos de documentos como los que se trabajan normalmente con la diferencia de que si se modifican o se quieren guardar, se debe generar otro archivo, es decir, evita que se modifique el original.

Este tipo de archivos se pueden descargar desde la misma página que las extensiones: https://extensions.libreoffice.org/

A continuación, se mostrará una plantilla para crear calendarios.


2.2.2.1 Creador de Calendario
+++++++++++++++++++++++++++++++

Con esta plantilla se podrá realizar un calendario personalizado por mes, para usarlo de referencia como así también para imprimirlo.

.. figure:: images/u6_libreoffice_calendar1.png
    :align: center
    :scale: 55 % 
    
.. figure:: images/u6_libreoffice_calendar2.png
    :align: center
    :scale: 55 % 
    
.. figure:: images/u6_libreoffice_calendar3.png
    :align: center
    :scale: 55 % 

El formato del archivo descargado es OTS (OpenDocument Spreadsheet Template).


2.3 Firefox (Navegador Web)
--------------------------------

Mozilla Firefox es un navegador web libre y de código abierto multiplataforma, desarrollado por la *Corporación Mozilla* y la *Fundación Mozilla*.

Si bien muchas veces sólo se utiliza el navegador para visitar páginas de internet o para descargar archivos, también se le puede agregar muchas funcionalidades para potenciar nuestro trabajo y/o nuestro tiempo navegando.

Estos aplicativos se llaman *Addons* y se pueden agregar tanto *Extensiones* como *Temas* al navegador Firefox.

La página web donde se pueden explorar estos aplicativos es: https://addons.mozilla.org/es/firefox/.


2.3.1 Extensiones de Firefox 
...............................

Las extensiones permiten agregar funcionalidades al navegador ya sea para optimizarlo como también para complementar las funciones que trae luego de la instalación. Página web: https://addons.mozilla.org/es/firefox/extensions/

A continuación se muestra la extensión "Adblock Plus".

2.3.1.1 Adblock Plus
+++++++++++++++++++++++

Esta extensión permite bloquear las ventanas emergentes de las páginas web, anuncios y demás anexos que contienen.

.. figure:: images/u6_firefox1.png
    :align: center
    :scale: 55 % 

Al hacer clic en "Agregar a Firefox", se comenzará con su instalación.  Se deberán dar los permisos requeridos para que pueda cumplir con su función:

.. figure:: images/u6_firefox2.png
    :align: center
    :scale: 55 % 

Al darle los permisos requeridos, se finalizará con su instalación.

.. figure:: images/u6_firefox3.png
    :align: center
    :scale: 55 %             

2.3.2 Temas de Firefox
........................

Los temas permiten cambiarle la apariencia a Firefox para dejarlo a nuestro agrado. Página web: https://addons.mozilla.org/es/firefox/themes/.

.. figure:: images/u6_firefox4.png
    :align: center
    :scale: 55 %    

Una vez elegido el tema, se debe hacer clic en "Instalar tema".

.. figure:: images/u6_firefox5.png
    :align: center
    :scale: 55 %    


2.4 Thunderbird (Lector de Correos)
--------------------------------------

Mozilla Thunderbird es un cliente de correo electrónico multiplataforma de código abierto y libre, cliente de noticias, cliente de RSS y de chat desarrollado por la Fundación Mozilla.

Al igual que Firefox, Thunderbird permite que se le agreguen funcionalidades a través de Extensiones y también cambiarle el aspecto visual para dejarlo como más nos guste.


2.4.1 Extensiones de Thunderbird
..................................

La página web donde se pueden buscar distintas extensiones para Thunderbird es: https://addons.mozilla.org/es/thunderbird/extensions/.


2.4.1.1 Thunderbird Conversations (Conversaciones)
++++++++++++++++++++++++++++++++++++++++++++++++++++

Esta extensión proporciona una vista de conversación para Thunderbird, agrupando los mensajes y permitiendo responder "inline", proporcionando así un flujo de trabajo más eficiente.

El procedimiento para instalar las extensiones es similar al realizado con el navegador Firefox. Sin embargo, esta vez se realizará dentro de la aplicación. Una vez abierta, se debe seleccionar "Complementos y temas" dentro de las opciones de Thunderbird.
	
.. figure:: images/u6_thunderbird1.png
    :align: center
    :scale: 55 %    

Se abrirá el "Administrador de complementos", en el cual una de las pestañas corresponde a las extensiones.

.. figure:: images/u6_thunderbird2.png
    :align: center
    :scale: 55 %    

Se puede utilizar el buscador para seleccionar la extensión deseada y luego instalarla. En todos los casos, se debe hacer clic en "Agregar a Thunderbird".

.. figure:: images/u6_thunderbird3.png
    :align: center
    :scale: 55 %    

Se procederá con su descarga y posterior instalación.

.. figure:: images/u6_thunderbird4.png
    :align: center
    :scale: 55 %    

Algunas extensiones requerirán permisos que se deben aceptar.

.. figure:: images/u6_thunderbird5.png
    :align: center
    :scale: 55 %    

Luego de aceptar, la extensión quedará instalada.

.. figure:: images/u6_thunderbird6.png
    :align: center
    :scale: 55 %    

Para verificar, se puede ir a la pestaña de las extensiones en el administrador:

.. figure:: images/u6_thunderbird7.png
    :align: center
    :scale: 55 %


2.4.2 Temas de Thunderbird
............................

Los temas permiten cambiarle la visualización a Thunderbird para dejarlo a nuestro agrado.

Dentro del "Administrador de complementos" de Thunderbird, se podrá encontrar una pestaña con los temas instalados por defecto.

.. figure:: images/u6_thunderbird_temas1.png
    :align: center
    :scale: 55 %   

Se podrá utilizar el buscador para instalar algún otro tema. Una vez seleccionado el tema, se debe hacer clic en "Agregar a Thunderbird". Como observarán, el procedimiento es similar al de instalar extensiones.

.. figure:: images/u6_thunderbird_temas2.png
    :align: center
    :scale: 55 %    
        

2.5 VLC (Reproductor Multimedia)
------------------------------------

VLC es un reproductor multimedia libre y de código abierto multiplataforma que reproduce la mayoría de archivos multimedia, así como DVD, Audio CD, VCD y diversos protocolos de transmisión.


2.5.1 Extensiones de VLC
.........................

Las extensiones permiten agregar funcionalidades al reproductor para complementar las funciones que trae luego de la instalación. 

Para instalarlas existen dos maneras: 

1) Descargarlas desde la página web (https://addons.videolan.org/) y luego copiar el archivo descargado dentro del directorio */home/<usuario>/.local/share/vlc/lua/extensions* (si los directorios *"lua"* y *"extensions"* no existen, se deben crear). Cuando se abra el VLC, la extensión ya figurará como instalada.

.. figure:: images/u6_vlc1.png
    :align: center
    :scale: 55 %
    
    Página principal de las extensiones de VLC
    
.. figure:: images/u6_vlc2.png
    :align: center
    :scale: 55 % 
    
    Directorio donde se copian las extensiones

2) Mediante la interfaz del VLC, seleccionando la opción "Complementos y extensiones" dentro del menú "Herramientas".

.. figure:: images/u6_vlc3.png
    :align: center
    :scale: 55 %

El "Administrador de complementos" mostrará las extensiones instaladas (se observa que aparece la extensión que se instaló por el método 1). 

Para buscar nuevas, se debe hacer clic en la opción "Buscar más complementos en línea".

.. figure:: images/u6_vlc4.png
    :align: center
    :scale: 55 %

En la parte derecha, se mostrarán los complementos disponibles. Al hacer clic sobre uno de ellos, permitirá que se instale.

.. figure:: images/u6_vlc5.png
    :align: center
    :scale: 55 %

A continuación, se instalará la extensión *VLSub* para mostrar el proceso.

2.5.1.1 VLSub
++++++++++++++++++

Esta extensión buscará y descargará los subtítulos desde http://www.opensubtitles.org/ del video en reproducción.

A continuación, se muestra cómo instalarla usando el "Administrador de complementos".

De la lista de complementos disponibles, buscar la extensión "VLSub" y luego hacer clic en "Instalar".

.. figure:: images/u6_vlc6.png
    :align: center
    :scale: 55 %

Se descarga automáticamente y cuando termina, se muestra como "Instalado". 

.. figure:: images/u6_vlc7.png
    :align: center
    :scale: 55 %

.. raw:: pdf

   PageBreak     
    


Bibliografía
========================================

*Goodies*. Online: http://goodies.xfce.org/

*LibreOffice*. Online: http://es.libreoffice.org/

*Extensiones de Libreoffice*. Online: https://extensions.libreoffice.org/

*Mozilla Firefox*. Online: https://www.mozilla.org/es-AR/firefox/

*Extensiones de Firefox*. Online: https://addons.mozilla.org/es/firefox/extensions/

*Mozilla ThunderBird*. Online: https://www.thunderbird.net/es-AR/

*Extensiones Thunderbird*. Online: https://addons.mozilla.org/es/thunderbird/extensions/

*VLC*. Online: https://www.videolan.org/vlc/

*Extensiones de VLC*. Online: http://addons.videolan.org/
