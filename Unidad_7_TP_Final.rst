================================================
Software Libre: Ofimática en las Organizaciones 
================================================
--------------------------------------------------------------------
Trabajo Práctico Final - 2022
--------------------------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Trabajo Práctico Final

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022.

:Autor: Leopoldo Bertinetti


.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak


Trabajo Práctico Final - 2022
==============================

Pautas generales
----------------------------

El trabajo práctico se deberá realizar de manera individual. Cada uno es libre de elegir la distribución GNU/Linux sobre la cual realizará el trabajo. No se permitirá que se utilice Debian Xfce.

El formato del archivo debe ser abierto (se recomienda ODT o PDF).

El nombre del archivo debe tener el siguiente formato: TP_FINAL_Apellido_Nombre.pdf o TP_FINAL_Apellido_Nombre.odt.

Se deberá subir el archivo del Trabajo Práctico a la tarea correspondiente dentro del aula ("Subir archivo Trabajo Práctico Final").

Se recomienda leer *"Primeros pasos con Writer"* para configurar el archivo según el formato que se solicita:

https://documentation.libreoffice.org/assets/Uploads/Documentation/es/GS62/PDF/GS6204-PrimerosPasosConWriter.pdf


Formato
------------

**Formato de papel:** A4

**Márgenes:**

- Superior: 3,0 cm.
- Inferior: 2,0 cm.
- Izquierdo: 3,0 cm.
- Derecho: 2,0 cm.

**Carátula:** Texto centrado, fuente Arial, sin numerar. Debe contener:

- Rótulo identificatorio de la Unidad Académica y la materia (tamaño 14pts): UNLVirtual - Facultad de Ingeniería y Ciencias Hídricas - Tecnicatura Universitaria en Software Libre - Software Libre: Ofimática en las Organizaciones.

- Título del Trabajo Práctico (iniciales en mayúsculas, negritas y tamaño 16pts): Trabajo Práctico Final - distribución seleccionada.

- Nombre y apellido del alumno (iniciales en mayúsculas, tamaño 14pts).

- Año de cursado (tamaño 12pts).

- Seleccionar una licencia Creative Commons (http://www.creativecommons.org.ar/licencias). **Ayuda:** https://creativecommons.org/choose/?lang=es_AR

A continuación se puede apreciar un modelo de la carátula:

.. figure:: images/u7_caratula_tpf.png
    :align: center
    :scale: 65 %

**Cada hoja del TPF:** debe contener:

- Cabecera de página o encabezado: Título del Trabajo Práctico y materia (iniciales en mayúsculas).

- Pie de página: Nombre y apellido del alumno (iniciales en mayúsculas). Además, en la esquina inferior derecha se debe introducir el número de página.

**Títulos:** Texto alineado a la izquierda, en negritas, fuente Arial y tamaño 14pts, con una separación del párrafo superior de 0,4 cm y del párrafo inferior de 0,4 cm.

**Subtítulos:** Texto alineado a la izquierda, en negritas y cursivas, fuente Arial y tamaño 12pts, con una separación del párrafo superior de 0,4 cm y del párrafo inferior de 0,4 cm.

**Párrafos:** Texto justificado, fuente Arial, tamaño 10pts, interlineado sencillo, con una sangría de 1,0 cm (en el primer renglón) y con una separación del párrafo superior de 0,2 cm y del párrafo inferior de 0,2 cm.


.. raw:: pdf

    PageBreak

    
    
Criterios de evaluación:
---------------------------

Se examinará:

    - Si se respetan las pautas enunciadas.
    - Si se responden todas las consignas solicitadas.
    - Si se respeta a los autores (citar en el caso de ser necesario).

    
Enunciado:
-----------

1. Teniendo en cuenta la distribución GNU/Linux seleccionada:

  - Nombre los requisitos mínimos de instalación de la misma.
  - Cree una máquina virtual (VM) nueva y su correspondiente disco virtual teniendo en cuenta los requisitos anteriores.
  - Instalar la distribución en la VM creada.
  - Se deberán crear al menos 2 particiones: una de ellas exclusiva para el directorio /home de los usuarios. Responder: ¿Qué beneficios brinda esta configuración?

2. Una vez finalizada la instalación, y previo a realizar modificaciones:

  - Realizar una captura de pantalla donde pueda apreciarse el fondo de escritorio y una ventana abierta de una aplicación que venga instalada por defecto en la distribución.
  - Adjuntar una captura de pantalla con el particionado realizado (abrir una terminal/consola y utilizar el comando *df -Th*. Para obtener más información/ayuda utilizar  el comando *man df*).
  - Adjuntar una captura de pantalla con la memoria RAM disponible en la VM (abrir una terminal/consola y utilizar el comando *free -h*. Para obtener más información/ayuda utilizar el comando *man free*).

3. Una vez realizadas las capturas anteriores, proceder a:

 - Cambiar el fondo de pantalla del escritorio.
 - Modificar el estilo o tema de escritorio.
 - Instalar una aplicación.
 - Instalar un plugin/complemento/addons de una aplicación, preferentemente Firefox en la distribución.

4. Mencionar de qué manera se realizaron todas las modificaciones solicitadas en el punto anterior y adjuntar una o dos capturas de pantalla con el resultado final.

5. Realizar una conclusión final sobre la experiencia adquirida utilizando/probando la distribución. **Nota:** mencionar en qué ámbito se podría utilizar; aspectos positivos y negativos de la distribución; evaluar el uso de los recursos de la computadora y rendimiento.
