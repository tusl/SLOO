
![Software Libre: Ofimática en las organizaciones](images/TSL_Software Libre- Ofimática en las organizaciones.png)


------------------------------------------------
Objetivos de la materia.
------------------------------------------------


* Esta materia debe ser la primera aproximación para pasar de ser un usuario de clicks a un usuario crítico lo que implica ser capaz de operar/manipular/hacer cosas con el software sobre la base de decisiones reflexionadas

* Ser capaz de probar e instalar GNU/Linux 

* Tener un panorama de las características de algunas distribuciones de GNU/Linux

* Poder instalar y configurar GNU/Linux en sus aspectos básicos de funcionamiento para una PC de escritorio en un ambiente de oficina

* Desarrollar la capacidad de crear máquinas virtuales (Vms)

* Probar distintas distribuciones de GNU/Linux mediante máquinas uso Vms

* Administrar paquetes con herramientas gráficas

* Instalar y configurar un entorno de escritorio completo. Esto implica conocer e entender diferencias themes, plugins, addons , etc


------------------------------------------------
Temario básico:
------------------------------------------------

**Unidad 0:** Introducción a la Computación (nivelación)

**Unidad 1:** Introducción a GNU/Linux

**Unidad 2:** Instalación de GNU/Linux

**Unidad 3:** Distribuciones de GNU/Linux

**Unidad 4:** Personalización

**Unidad 5:** Entornos

**Unidad 6:** Modificación y personalización de aplicaciones

**Unidad 7:** Trabajo Práctico Final


------------------------------------------------
Instrucciones para generar los PDFs
------------------------------------------------

Paquetes requeridos: *rst2pdf* y *pdftk*

**Instalación:**

    sudo apt-get install rst2pdf pdftk

**Pasos:**

1) Ubicarse en el directorio raíz del año académico

2) Ejecutar el siguiente comando:

    rst2pdf -s tusl.style Unidad_X.rst -o pdfs/TUSL_SLOO_Unidad_X_SP.pdf

donde *X* debe ser reemplazada por el número de la unidad temática. *SP* significa "sin portada"

3) Para agregar la portada al PDF de la unidad, ejecutar: 

    pdftk portadas/Unidad_X_portada.pdf pdfs/TUSL_SLOO_Unidad_X_SP.pdf cat output pdfs/TUSL_SLOO_Unidad_X.pdf
