================================================
Software Libre: Ofimática en las Organizaciones 
================================================
--------------------------------------------------------------------
Unidad 3: Distribuciones GNU/Linux
--------------------------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Unidad 3 - Distribuciones GNU/Linux

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022.

:Autor: Leopoldo Bertinetti


.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

.. contents:: Contenido

.. raw:: pdf

    PageBreak



Módulo 1: ¿Qué es un Sistema Operativo?
========================================

El Sistema Operativo es el software que permite controlar el hardware de una manera eficiente para que se puedan realizar las tareas de forma cómoda. El sistema operativo presenta al usuario un entorno en el que se pueden ejecutar aplicaciones para llevar a cabo diversas tareas, gestionando los recursos disponibles (CPU, memoria, disco rígido, comunicaciones en red, dispositivos periféricos, etc.). De este modo, un usuario puede realizar tareas como abrir un archivo y acceder a la información que contiene sin preocuparse de aspectos de bajo nivel como: mover los cabezales del disco, la velocidad de giro del motor del disco, la posición que ocupan los bits en la superficie del disco, etc.



.. figure:: images/u3_so1.png
    :align: center
    :scale: 90 %


Las funciones primarias de un sistema operativo son:
 
* **Abstracción:** Las aplicaciones no deben preocuparse de los detalles de acceso al hardware, o de la configuración particular de una computadora. El sistema operativo se encarga de proporcionar una serie de abstracciones para que los programadores puedan enfocarse en resolver las necesidades particulares de sus usuarios. Un ejemplo de tales abstracciones es que la información está organizada en archivos y directorios (en uno o muchos dispositivos de almacenamiento).
 
 
* **Administración de recursos:** Una computadora puede tener a su disposición una gran cantidad de recursos (memoria, espacio de almacenamiento, tiempo de procesamiento, etc.), y los diferentes procesos que se ejecuten en él compiten por ellos. Al gestionar toda la asignación de recursos, el sistema operativo puede implementar políticas que los asignen de forma efectiva y acorde a las necesidades establecidas para dicho sistema.


* **Aislamiento:** En un sistema multiusuario y multitarea cada proceso y cada usuario no tendrá que preocuparse por otros que estén usando el mismo sistema. Idealmente, su experiencia será la misma que si el sistema estuviera exclusivamente dedicado a su atención (aunque fuera un sistema menos potente). Para implementar correctamente las funciones de aislamiento hace falta que el sistema operativo utilice hardware específico para dicha protección.


1.1 Componentes de un Sistema Operativo
------------------------------------------

En un sistema operativo moderno se pueden encontrar las siguientes capas (del más alto nivel al más bajo):
 
- **Aplicaciones de usuario:**

Comprende una amplia gama de programas que permiten al usuario realizar diferentes tareas como hacer cálculos, gráficos, escuchar música, navegar por internet, etc. Ejemplos: Gimp, Firefox, LibreOffice.
 
- **Intérprete de comandos / Interfaz gráfica de usuario:**

Permiten la comunicación entre el sistema y el usuario. Existen distintas posibilidades, tanto de intérprete de comandos (*bash* [#]_, *csh* [#]_, *ksh* [#]_, etc.) como de interfaz gráfica (X Window, Gnome, KDE, MATE). Estos últimos entornos se verán en la *Unidad 5: Entornos gráficos de escritorio*.
 
.. [#] Bash: https://es.wikipedia.org/wiki/Bash
.. [#] Csh: https://es.wikipedia.org/wiki/C_Shell
.. [#] Ksh: https://es.wikipedia.org/wiki/Korn_shell

 
- **Llamadas al sistema:**

Son bibliotecas con funciones de uso frecuente, de tal manera que otros componentes software las puedan utilizar sin tener que volver a implementarlas (funciones matemáticas, de acceso a ficheros, comunicaciones, procesado gráfico, etc.).
 
- **Núcleo o kernel:**

Controla el hardware y las operaciones básicas que realiza el sistema operativo. Típicamente se encarga de la gestión de procesos, la gestión de memoria y el control de todos los elementos hardware.
 
- **Capa de Abstracción del Hardware** (**HAL**, del inglés *Hardware Abstraction Layer*):

Se encarga de independizar los detalles de la arquitectura hardware para el resto de los componentes. Forma parte del núcleo. 


.. figure:: images/u3_so2.png
    :align: center
    :scale: 78 %



Módulo 2: ¿Qué es una Distribución?
====================================


En general, una distribución es considerada un sistema operativo completo en el cual existe un conjunto de software que ha sido seleccionado con cierto propósito específico. Este software puede estar definido por el tipo de usuario que lo utilizará, por las características técnicas de los dispositivos donde se ejecutará y, por supuesto, por las convergencias políticas de dicho sistema operativo.
 
Una distribución está compuesta por un kernel o núcleo, herramientas y bibliotecas, software adicional, documentación, un sistema de ventanas, un administrador de ventanas y un entorno de escritorio. Varios de estos conceptos se retomarán en la *Unidad 5: Entornos gráficos de escritorio*.
 
En la mayoría de las distribuciones, el kernel se llama Linux pero también existen otros sistemas como el denominado “Hurd”, que se puede resumir como una colección de protocolos que interactúan por medio de programas servidores. Véase, "*Hurd: el kernel que no fue*" [#]_. 

.. [#] Hurd: el kernel que no fue: http://blog.desdelinux.net/hurd-el-kernel-que-no-fue/
 
Además del núcleo Linux, las distribuciones incluyen habitualmente las bibliotecas y herramientas del proyecto GNU y el *sistema de ventanas X* (en inglés, *X Window System*) [#]_. En este punto, hace falta añadir las restantes capas del sistema que permiten controlar tanto el hardware como el software de la computadora. Aquí es donde surgen las distribuciones: sistemas operativos que usan el kernel Linux y herramientas GNU pero disponen de su propia interfaz y gestión de los recursos. A su vez, dependiendo del tipo de usuarios a los que la distribución esté dirigida se incluye también otro tipo de software como procesadores de texto, hoja de cálculo, reproductores multimedia, herramientas administrativas, etc.

.. [#] Sistema de Ventanas X: https://es.wikipedia.org/wiki/Sistema_de_ventanas_X

Es así cómo se originan las distintas **distribuciones GNU/Linux**.
 
“*Una distribución GNU/Linux tiene dos objetivos principales: instalar un sistema operativo libre en un equipo (sea con o sin uno o más sistemas preexistentes) y proveer un rango de programas que cubran todas las necesidades del usuario*".  Texto extraído de “*El papel de las distribuciones*" [#]_.

.. [#] El papel de las distribuciones: https://debian-handbook.info/browse/es-ES/stable/sect.role-of-distributions.html
 




Módulo 3: Categorización de Distribuciones de GNU/Linux
=========================================================

Desde 1992 existen muchas distribuciones de GNU/Linux y cada una de ellas tiene un sentido que las hace existir, migrar, forkear (crear derivados o bifurcaciones) y hasta descontinuar o abandonar.
 
En el siguiente enlace se puede encontrar un catálogo actualizado de las distintas distribuciones, donde se podrán monitorear todas aquellas que vayan surgiendo: http://distrowatch.com/. Además, dicha página web brinda un ranking de popularidad de cada una de las distribuciones. 
 
En esta sección se tratará de reconocer el porqué de tantas distribuciones y se analizarán algunos aspectos que llevan a caracterizar las mismas.
 
Para ello se propone discutir los siguientes grandes grupos de distribuciones: según sus libertades, características técnicas, tipos de paquetes, tipos de usuarios, liberación de sus actualizaciones y según el soporte que reciben.


3.1 Por Libertades
--------------------

Como se comentó en la introducción de la Unidad 1, la mayoría de distribuciones no está avalada por la Free Software Foundation (FSF). 
 
Según la FSF [#]_, las distribuciones que no son consideradas como libres no respetan alguno de los siguientes dos aspectos importantes:

.. [#] Por qué no avalamos otros sistemas: https://www.gnu.org/distros/common-distros.es.html

* “*No adoptan ninguna política para incluir únicamente software libre, ni para eliminar el software que no sea libre cuando se detecta.*”

* “*El núcleo que distribuyen (en la mayoría de los casos, Linux) incluye blobs: piezas de código compilado distribuidas sin el código fuente, generalmente firmware para hacer funcionar algún dispositivo.*”

Dentro de las pautas para distribuciones libres [#]_, se pueden destacar las siguientes:

.. [#] Pautas para distribuciones de sistemas libres: https://www.gnu.org/distros/free-system-distribution-guidelines.es.html

 
*“Toda la información de uso práctico en una distribución debe estar disponible en el formato fuente («fuente» significa el formato de la información que se usa para modificarla). Toda la información, y su fuente, debe publicarse bajo una licencia libre apropiada.”*
 
*“Una distribución de sistema libre no debe dirigir a los usuarios a obtener información de uso práctico que no sea libre, ni debe incentivarlos para que lo hagan. El sistema no debe tener repositorios con software privativo ni instrucciones específicas para la instalación de programas que no sean libres. Tampoco la distribución debe remitir a repositorios de terceros que no se hayan comprometido a incluir únicamente software libre; aunque quizá no tengan software privativo hoy, lo podrían tener mañana. Los programas incluidos en el sistema no deben sugerir la instalación de programas complementarios [plugins], documentación u otros elementos que no sean libres.”*
 
*“Algunas aplicaciones y controladores requieren el uso de firmware para poder funcionar, y a veces ese firmware se distribuye solo en forma de código compilado bajo una licencia que no es libre. A estos programas de firmware los llamamos «blobs» (objetos binarios). En la mayoría de los sistemas GNU/Linux, típicamente los encontrará acompañando a algunos controladores en el núcleo Linux. Tal firmware debe eliminarse de una distribución de sistema libre.”*
 
*“Toda la documentación de una distribución de sistema libre debe publicarse bajo una licencia libre apropiada. Además, no debe recomendar software que no sea libre.”*
 
*“La distribución no debe contener gestión digital de restricciones (DRM) ni puertas traseras ni software espía (spyware).”*
 
En el siguiente enlace se puede observar un listado de las **distribuciones libres**: https://www.gnu.org/distros/free-distros.es.html


3.2 Por Características Técnicas
-----------------------------------

La mayoría de las distribuciones están pensadas para funcionar de manera óptima en un ambiente técnico determinado. Este ambiente está dado por las características mínimas del equipo (hardware).
 
Muchas de las distribuciones más conocidas tratan de mantener su software actualizado y configurado de manera que sean fáciles de utilizar por usuarios que no poseen conocimientos técnicos de ningún tipo. En general, estas distribuciones son de **uso masivo** y tienen mucha difusión; esto lleva a que las comunidades también sean masivas y, por lo tanto, a que los usuarios accedan más rápidamente a soluciones de problemas comunes, generalmente mediante simples búsquedas en internet.

Sin embargo, no todos los usuarios tienen acceso a hardware con los requerimientos mínimos para el uso correcto. Por esta razón, las distribuciones más difundidas no son completamente funcionales para estos usuarios. Esto, sumado a la capacidad del software libre frente a la *obsolescencia programada* [#]_, permiten el surgimiento de distribuciones con menos requisitos de hardware.

.. [#] La obsolescencia programada es definida generalmente como la determinación o programación del fin de la vida útil de un producto. Tras ese tiempo calculado de antemano por el fabricante, el producto se torna obsoleto, inútil o inservible. En informática, es un término complejo que no sólo depende del hardware y el software, sino también de costumbres y necesidades sociales. Está instaurado en todos los usuarios (por motivos de seguridad, compatibilidad y otros) que lo recomendado es poseer las últimas actualizaciones de equipos y software para que todo funcione correctamente. Pensar de esta manera nos hace creer que son las tecnologías las que generan los cambios en las sociedades.

Las **distribuciones livianas**, como suelen llamarse, también tienen otros orígenes. Algunas de ellas están configuradas para determinado hardware, como computadoras de usos específicos, entre las que pueden mencionarse CIAA/Linux para Computadora Industrial Abierta Argentina (CIAA), Raspbian para Raspberry pi, entre otras. También pueden distinguirse Puppy Linux, Damn Small Linux, entre muchas más que están desarrolladas con el fin de funcionar en computadoras de escritorio con pocos recursos o también tener la posibilidad de utilizar su sistema operativo con software libre donde sea necesario.

Entre otros aspectos técnicos, en el desarrollo de una distribución se debe tener en cuenta el consumo de recursos de las aplicaciones. Por ejemplo, en cierto momento se debían considerar las bibliotecas de software que utilizaba cada aplicación; se utilizaban las librerías gráficas GTK o QT y, por lo tanto, algunas distribuciones de software estaban armadas de manera de reutilizar estas librerías para que consumieran menos memoria y menos espacio de almacenamiento.
 
Otras distribuciones tratan de mantener cierta compatibilidad entre los diferentes hardware comportándose de manera similar con características similares. Uno de ellos es el caso de Debian, el cual tiene **soporte para distintas arquitecturas de procesadores**. Véase, "*Debian, Adaptaciones a otras arquitecturas*"[#]_.

.. [#] Debian, Adaptaciones a otras arquitecturas: https://www.debian.org/ports/index.es.html.

3.3 Por Formato de Paquetes 
-----------------------------

En el gráfico (https://es.wikipedia.org/wiki/Anexo:Distribuciones_Linux#/media/File:Gldt1202.svg) pueden observarse tres grandes ramas de distribuciones, clasificadas según sus tipos de paquetes: 
 
* **deb:** La primera rama es la de los derivados de Debian, que continúan con paquetes con extensión .deb (en los que los archivos del paquete están "archivados" en un único archivo junto a información del software y sus dependencias). 

* **rpm:** Otra rama muy utilizada es la de los paquetes rpm, sistema creado por la empresa Red Hat donde el sistema también es capaz de instalar, actualizar, desinstalar, verificar y solicitar cualquier software que se encuentre en los repositorios.

* **tgz, txz o tar.gz:** La última gran rama que aparece en el gráfico es la del sistema de paquetes nativo de Slackware, cuya diferencia principal se debe a que no hace control de las dependencias.
 
En menor medida, también se pueden mencionar los paquetes **pkg.tar.xz** o **pkg.tar.gz**, el formato estándar de Arch y derivadas, entre ellas Chakra, Manjaro o Antergos. Estos paquetes son binarios precompilados.


3.4 Por Tipo de Usuario
-------------------------

Existen distribuciones que fueron creadas a partir de una necesidad puntual de los diferentes usuarios. No es difícil pensar los distintos usos que se le dan a las computadoras de escritorio y, por ende, las distintas distribuciones que pueden ser creadas para satisfacer esos usos. 
 
Un ejemplo de este tipo de distribuciones es Etertics GNU/Linux, pensada para el uso en radios que sólo prevé utilizar software libre para sus transmisiones. Aquí puede observarse una convergencia entre lo técnico y lo político, que es lo que define que sea 100% libre.
 
Otro caso similar es Musix, distribución definida como sistema operativo multimedia totalmente libre. De igual modo, pueden encontrarse **distribuciones especializadas** en ediciones de fotos y video, traducciones, educación, etcétera.
 
También es importante reconocer otros tipos de **distribuciones que no son definidas para usuarios finales**. Estas distribuciones tienen como propósito poner en funcionamiento algún servicio o aplicación para su uso en una red, como ser un servidor de archivos, impresión, de páginas web, etc. Existen muchos ejemplo de este tipo de plataformas como ser Ubuntu Server para uso general, Zentyal para servidor de archivos, correos, VPN, entre otros servicios y pfsense para seguridad de la red.


3.5 Por Ciclo de Liberación de Actualizaciones
------------------------------------------------
 
Las distribuciones también se pueden clasificar según dos tipos diferentes de ciclos de lanzamiento de sus actualizaciones: **versiones estándar** y **lanzamientos sucesivos**.
 
Muchos usuarios prefieren una liberación continua, o **rolling release**, para tener el software más reciente, mientras que otros eligen las versiones estándar por ser más estables y probadas.
 
Se puede mencionar como ejemplo a Ubuntu. Esta distribución tiene un lanzamiento de nuevas versiones cada 6 meses. En ese plazo de tiempo, se generan actualizaciones de paquetes nuevos para la versión posterior por lo que se pueden presentar los siguientes problemas: 

* Se deben cambiar los repositorios cada 6 meses.

* Instalar o actualizar sobre la versión ya instalada puede provocar errores.

* Los paquetes de la versión anterior se van quedando obsoletos rápidamente. 

Por estas razones, se recomienda siempre hacer una instalación en limpio, desde cero.
 
Las distribuciones “rolling release” tratan de solucionar la situación anterior. Por ejemplo, cuando un usuario instala Arch Linux ya no necesitará reinstalarlo (salvo que se presente un problema serio en el sistema). Una vez instalados todos los paquetes necesarios, se van actualizando continuamente a sus últimas versiones.
  
Sin embargo, este tipo de distribuciones tiene las siguientes desventajas:
 
* El sistema puede ser inestable, debido a que si bien se disponen de las últimas versiones de todos los paquetes, por esta misma razón, también se tratan de las versiones menos testeadas.

* Al no lanzarse actualizaciones completas de la distribución muy a menudo, al instalarla se tendrá que actualizar una mayor cantidad de paquetes.
 
Dichas desventajas, se contraponen con las ventajas de las distribuciones que liberan versiones cada cierto tiempo: son más estables y probadas, y al instalar una de este tipo, no se deben actualizar todos los paquetes.
 

3.6 Por Soporte
----------------
 
Otra categorización de las distribuciones GNU/Linux puede estar dada por quienes dan soporte a las mismas. Es así que se pueden distinguir aquellas que están **soportadas comercialmente**, como Fedora por la empresa Red Hat, openSUSE por Novell y Ubuntu por Canonical Ltd. por nombrar algunas.
 
Por otra parte, existen distribuciones **mantenidas por la comunidad** como Debian y Gentoo; y otras que no están relacionadas con ninguna empresa o comunidad, como es el caso de Slackware.



Bibliografía
=============

*Apuntes del curso ''Sistemas operativos monousuario y multiusuario''*. Instituto Puig Castellar de Santa Coloma de Gramanet, Barcelona, España. Online: https://elpuig.xeill.net/Members/vcarceler/c1

*Fundamentos de sistemas operativos*. Gunnar Wolf, Esteban Ruiz, Federico Bergero y Erwin Meza. Universidad Nacional Autónoma de México, Instituto de Investigaciones Económicas, Facultad de Ingeniería, 2015. Online: http://sistop.org/pdf/sistemas_operativos.pdf

*Página oficial de la Free Software Foundation (FSF)*. Online: https://fsf.org/

*Sitio Oficial de Debian*. Online: https://www.debian.org/

*Debian Handbook*. Online: https://debian-handbook.info/

*Sitio Oficial del proyecto GNU*. Online: https://www.gnu.org/

*Distribución Linux*. Online: https://es.wikipedia.org/wiki/Distribuci%C3%B3n_Linux

*Anexo: Distribuciones Linux*. Online: https://es.wikipedia.org/wiki/Anexo:Distribuciones_Linux 

*Introducción a Linux*. José Juan Grimaldos Parra y Antonio Saorín Martínez. Instituto Nacional de Tecnologías Educativas y de Formación del Profesorado. Ministerio de Educación, Cultura y Deportes. Gobierno de España. Online: http://www.ite.educacion.es/formacion/materiales/43/cd/indice.htm 
 
*GNU/Etertics*. Online: https://gnuetertics.org/get-en-pocas-palabras.php

*Musix*. Online: https://musixdistro.wordpress.com/

*Servidores en Linux*. Online: http://www.linux-es.org/node/1832

*Pacman*. Online: https://wiki.archlinux.org/index.php/Pacman 
 
*Red Hat*. Online: https://es.wikipedia.org/wiki/Red_Hat
 
*OpenSUSE*. Online: https://es.wikipedia.org/wiki/OpenSUSE

*Slackware*. Online: https://es.wikipedia.org/wiki/Slackware

*Las mejores distribuciones rolling-release*. Online: https://blog.desdelinux.net/las-mejores-distribuciones-rolling-release/

