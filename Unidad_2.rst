================================================
Software Libre: Ofimática en las Organizaciones 
================================================
------------------------------------------------
Unidad 2: Instalación de GNU/Linux
------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Unidad 2 - Instalación de GNU/Linux

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022.

:Autor: Leopoldo Bertinetti

.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

.. contents:: Contenido

.. raw:: pdf

    PageBreak





Módulo 1: Instalación de GNU/Linux Debian en VirtualBox
============================================================

En esta unidad se mostrará el proceso de instalación de Debian 11 Xfce.

En la Unidad 1 se mencionó que VirtualBox permite crear máquinas virtuales (VM) a las cuales se les puede asignar recursos del sistema: procesadores, memoria RAM, disco rígido (espacio de almacenamiento), placas de red y otros recursos. No se pretende profundizar en conocimientos de virtualización, sino contar con un entorno seguro de trabajo que facilite la prueba e instalación de distintos Sistemas Operativos GNU/Linux.

1.1 Creación de máquina virtual con disco
---------------------------------------------------------------------

En esta sección se creará una máquina virtual (VM) y se le asignará un disco duro virtual. Tener en cuenta que también se podría agregar directamente el disco duro virtual a la VM ya creada en la unidad anterior.

En primer lugar, se debe abrir la aplicación *VirtualBox* y luego elegir la opción "Nueva" para crear una nueva VM.

.. figure:: images/u2_vbox1.png
    :align: center
    :scale: 54 %

El siguiente paso es colocar nombre a la VM y especificar el tipo de sistema operativo. En *Tipo* seleccionar "Linux" y en *Versión*, "Debian (32-bit)":

.. figure:: images/u2_vbox2.png
    :align: center
    :scale: 54 %

A continuación, se asigna la cantidad de memoria RAM que utilizará como máximo la VM. Esta cantidad está limitada por la memoria real presente en su equipo (se recomienda no asignar más de la mitad de la memoria real).

.. figure:: images/u2_vbox3.png
    :align: center
    :scale: 54 %

La siguiente pantalla permite crear un disco duro para instalar el sistema operativo GNU/Linux. En esta ocasión se seleccionará "Crear un disco virtual ahora". Un disco virtual es un archivo que permitirá almacenar el sistema operativo y los archivos que se generen dentro de él. Luego, hacer clic en "Crear".

.. figure:: images/u2_vbox4.png
    :align: center
    :scale: 54 %

Se deberá seleccionar el tipo de archivo de disco duro. Se dejará seleccionada la primera opción "VDI (VirtualBox Disk Image)".

.. figure:: images/u2_vbox5.png
    :align: center
    :scale: 54 %

El siguiente paso es establecer el tipo de almacenamiento que utilizará el disco virtual. Pueden ser de dos tipos:

- *Reservado dinámicamente:* indica que el disco virtual utilizará espacio en el disco físico a medida que se necesite hasta un máximo (tamaño fijo) que se definirá en un paso posterior.

- *Tamaño fijo:* el disco virtual ocupará todo el espacio fijado en el disco físico.

.. figure:: images/u2_vbox6.png
    :align: center
    :scale: 54 %

Se dejará seleccionada la opción "Reservado dinámicamente". Clic en "Siguiente".

En la siguiente pantalla se debe elegir donde se guardará el archivo VDI del disco virtual y el tamaño máximo que podrá tener. Se recomienda un tamaño de 25 GB.

.. figure:: images/u2_vbox7.png
    :align: center
    :scale: 54 %

Una vez que se ha finalizado la creación del disco y de la VM, se está en condiciones de comenzar con la instalación.

.. figure:: images/u2_vbox8.png
    :align: center
    :scale: 54 %


1.2 Instalación del Sistema Operativo GNU/Linux: Debian
---------------------------------------------------------------------

Continuando con el enfoque práctico de la materia, en esta ocasión se realizará la instalación de un sistema operativo GNU/Linux en un entorno seguro de trabajo (máquina virtual).

1.2.1 Inicio de la máquina virtual
.............................................................

Para instalar el sistema operativo, lo primero que se debe hacer es iniciar la máquina virtual.

Por defecto, se solicita bootear desde la unidad de CD/DVD o desde un archivo imagen de un disco (archivo con extensión .ISO). Para seleccionar el archivo se debe presionar el ícono que se indica en la imagen siguiente:

.. figure:: images/u2_vbox9.png
    :align: center
    :scale: 54 %


1.2.2 Elección ISO / CD / DVD
.............................................................

El siguiente paso es utilizar el archivo descargado con la imagen de Debian que se deberá buscar dentro de nuestra estructura de directorios:

.. figure:: images/u2_vbox10.png
    :align: center
    :scale: 54 %


1.2.3 Inicio desde el archivo ISO
.............................................................

Una vez que se ha seleccionado el archivo ISO, se debe hacer clic en "Iniciar" para que continúe y pueda cargarse el instalador.

.. figure:: images/u2_vbox11.png
    :align: center
    :scale: 54 %

    
1.2.4 BIOS de VirtualBox
.............................................................

Se verá la pantalla de la BIOS de VirtualBox, donde se permite elegir desde dónde iniciar (tecla *F12*). Por defecto, se iniciará desde el archivo ISO que se seleccionó en el paso anterior.

.. figure:: images/u2_vbox12.png
    :align: center
    :scale: 70 %

    
1.2.5 Inicio de la instalación de Debian
.............................................................

Existen dos formas de instalar Debian mediante el archivo ISO descargado. La primera es con su clásico instalador que corresponde a la tercera opción que se observa en la imagen (*"Graphical Debian Installer"*):

.. figure:: images/u2_debian01.png
    :align: center
    :scale: 70 %

La otra manera de instalar Debian es con el instalador *"Calamares"*, para lo cual debemos seleccionar la primera de las opciones (iniciar Debian "en vivo"). Este procedimiento es el que se verá en este apunte.

Una vez que se haya iniciado el sistema, se debe hacer clic en el ícono "Install Debian" que aparece en el Escritorio.

.. figure:: images/u2_debian02.png
    :align: center
    :scale: 60 %

    
1.2.6 Idioma de instalación 
.............................................................

Como se ve en la siguiente imagen, se abre el instalador *Calamares*, que nos guiará durante todo el proceso de instalación de Debian.

La primera configuración que se debe realizar es seleccionar el idioma correspondiente:

.. figure:: images/u2_debian03.png
    :align: center
    :scale: 60 %
    

1.2.7 Ubicación y zona horaria
.............................................................

En la siguiente pantalla, se solicita nuestra ubicación. Así el instalador podrá determinar el huso horario y definir el servidor de actualizaciones más próximo.

.. figure:: images/u2_debian04.png
    :align: center
    :scale: 60 %    
    
    
1.2.8 Distribución del teclado 
.............................................................

A continuación, se debe elegir la distribución del teclado (disposición y presencia de las teclas).

.. figure:: images/u2_debian05.png
    :align: center
    :scale: 60 %   
    

1.2.9 Particiones
.............................................................

En esta sección, se elige el dispositivo de almacenamiento y la forma (particionado) donde se instalará Debian. Para realizar el proceso de manera sencilla se utilizará la opción "Borrar disco". De esa forma, las particiones se generarán automáticamente.

**Nota:** Observar que está disponible la opción *"Particionado manual"*, donde se podrán crear particiones y redimensionarlas a nuestra voluntad.

.. figure:: images/u2_debian06.png
    :align: center
    :scale: 60 %   

Al seleccionar la opción mencionada ("Borrar disco"), se muestra cómo quedará particionado el disco duro (virtual). 

.. figure:: images/u2_debian07.png
    :align: center
    :scale: 60 %   

Clic en "Siguiente" para continuar.

    
1.2.10 Usuarios
.............................................................

En el siguiente paso se deben configurar los siguientes datos:

* **Nombre**: se utiliza sólo como información para el sistema.
* **Nombre de usuario**: es la identificación que se utiliza ante el sistema.
* **Nombre del equipo**: sirve para identificar el equipo dentro de la red en la que se encuentra.
* **Contraseña**: es una clave que permite al usuario acceder al sistema.

.. figure:: images/u2_debian08.png
    :align: center
    :scale: 60 %  
    
Una vez que se completen todos los datos, se habilitará la opción "Siguiente".


1.2.11 Resumen
.............................................................

En esta sección se muestra un resumen de todas las configuraciones que hemos realizado hasta este momento. Luego de verificar que están correctas, se debe hacer clic en "Instalar".

.. figure:: images/u2_debian09.png
    :align: center
    :scale: 60 %  
    
    
1.2.12 Instalación desatendida
.............................................................

Llegando a este punto, la instalación es completamente desatendida; se instalará lo necesario sin requerir nuestra interacción.

.. figure:: images/u2_debian10.png
    :align: center
    :scale: 60 %  


1.2.13 Reinicio de la máquina virtual
.............................................................

Al finalizar la instalación del sistema, se solicitará que se reinicie el equipo, que en este caso será la máquina virtual. Clic en "Hecho".

.. figure:: images/u2_debian11.png
    :align: center
    :scale: 60 % 
    
    
1.2.14 Quitar o expulsar CD/DVD (Desmontar ISO)
.............................................................

Luego de haber reiniciado, posiblemente se seguirá mostrando el menú de la imagen ISO que habíamos seleccionado en pasos anteriores. Esto se debe a que quedó asignada en la VM. Para poder iniciar Debian desde el disco donde lo instalamos, se deberá quitar el archivo ISO. Para eso se debe ir a la configuración de la VM (la VM debe estar apagada) y en la sección "Almacenamiento" se debe quitar la conexión "debian-live-11.3.0-i386-xfce.iso" (haciendo clic con el botón derecho).

.. figure:: images/u2_debian12.png
    :align: center
    :scale: 54 % 

Una vez guardados los cambios, se iniciará la VM y podremos iniciar Debian desde el disco duro donde se instaló.

.. figure:: images/u2_debian13.png
    :align: center
    :scale: 70 % 

La primera pantalla que nos aparece es la del login, en la cual deberemos ingresar los datos que habíamos consignado durante la instalación.

.. figure:: images/u2_debian14.png
    :align: center
    :scale: 60 % 
    
Así ha concluido una instalación exitosa del sistema operativo GNU/Linux Debian. Si bien se ha realizado mediante VirtualBox, el proceso de instalación en la computadora física se realiza de manera similar.

.. figure:: images/u2_debian15.png
    :align: center
    :scale: 60 % 

    
Módulo 2: Gestores de paquetes
===================================================================

Debian emplea el formato de paquete denominado **.deb**, y para gestionar su sistema de paquetes dispone de varias herramientas:  

2.1 APT (Advanced Package Tool)
------------------------------------------------------------

APT son las siglas de "herramienta avanzada de paquetes". Es un sistema de paquetes de software que no sólo evalúa individualmente cada paquete sino que los considera como un todo y produce la mejor combinación posible de paquetes dependiendo de lo que esté disponible y sea compatible (según dependencias).

El sistema incluye herramientas para gestionar una base de datos de paquetes que nos permite obtenerlos e instalarlos, detectando y solucionando problemas de dependencias y conflictos entre paquetes.

Existen 2 "frontend", basados en la línea de comandos: **apt-get** y **apt**. Ambas herramientas están creadas a partir de la misma biblioteca y son, por lo tanto, muy similares.

En esta materia no se profundizará este tema. Para más información, véase:  *"6.2. Los programas aptitude, apt-get y apt"* [#]_

.. [#] 6.2. Los programas aptitude, apt-get y apt: https://debian-handbook.info/browse/es-ES/stable/sect.apt-get.html


2.2 Synaptic
------------------------------------------------------------

En esta materia se utilizará el sistema de instalación y actualización llamado "Synaptic" debido a que es el sistema gráfico más utilizado en las distribuciones basadas en paquetes *.deb*.

"Synaptic" es una herramienta gráfica para la gestión de paquetes basada en gtk+ y APT. Permite instalar, actualizar y desinstalar paquetes de programas de forma amigable. Además permite añadir repositorios de terceros para instalar aplicaciones que no se encuentren en los repositorios oficiales de Debian.

Este software se puede ejecutar desde el menú de aplicaciones, en la categoría "Sistema":

.. figure:: images/u2_debian_synaptic.png
    :align: center
    :scale: 60 % 

**Nota:** Al abrir "Synaptic" siempre se solicitará nuestra contraseña. 

.. figure:: images/u2_debian_synaptic02.png
    :align: center
    :scale: 60 %
    
La primera vez que se inicie, mostrará un mensaje con una pequeña introducción sobre los paquetes y el modo de utilizar la aplicación.

.. figure:: images/u2_debian_synaptic03.png
    :align: center
    :scale: 60 %

.. figure:: images/u2_debian_synaptic04.png
    :align: center
    :scale: 60 %
    
    
2.2.1 Actualizaciones del sistema
.............................................................

A continuación se utilizará Synaptic para actualizar todo el sistema. Siempre es recomendable actualizar el sistema luego de una instalación dado que los paquetes instalados desde el ISO siempre son versiones más antiguas que las actuales. Por otra parte, también se sugiere realizar actualizaciones periódicas por cuestiones de seguridad, por nuevas funcionalidades o por corrección de errores.

Entonces, como primer paso, se debe hacer clic en la opción "Recargar".

.. figure:: images/u2_debian_synaptic05.png
    :align: center
    :scale: 60 %

Como se puede observar en la imagen, la opción anterior servirá para obtener información de paquetes nuevos, eliminados y/o actualizados de los repositorios de Debian.

.. figure:: images/u2_debian_synaptic06.png
    :align: center
    :scale: 60 %

Cuando termina, dentro de la pestaña "Estado", opción "Instalado (actualizable)" se podrán identificar todos los paquetes que se pueden actualizar.

.. figure:: images/u2_debian_synaptic07.png
    :align: center
    :scale: 60 %

Luego, se debe hacer clic en el botón "Marcar todas las actualizaciones".

.. figure:: images/u2_debian_synaptic08.png
    :align: center
    :scale: 60 %

Saldrá un mensaje para que confirmemos los cambios adicionales que se producirán.

.. figure:: images/u2_debian_synaptic08_2.png
    :align: center
    :scale: 60 %

Cuando estén marcadas todas las actualizaciones, se debe hacer clic en el botón "Aplicar".

.. figure:: images/u2_debian_synaptic10.png
    :align: center
    :scale: 60 %

En la siguiente pantalla se muestra un resumen con los paquetes que se actualizarán y los nuevos que se instalarán. Para confirmar, se debe hacer clic en "Apply".
    
.. figure:: images/u2_debian_synaptic11.png
    :align: center
    :scale: 60 %

Se comenzará con la descarga e instalación de los paquetes mencionados:

.. figure:: images/u2_debian_synaptic12.png
    :align: center
    :scale: 60 %

Al finalizar, se nos informa que los cambios están aplicados. De esa manera, se concluye con las actualizaciones de todos los paquetes del sistema.

.. figure:: images/u2_debian_synaptic13.png
    :align: center
    :scale: 60 %
    
2.2.2 Instalación de aplicaciones (Ejemplo: VLC)
.............................................................

Para mostrar de forma práctica cómo utilizar Synaptic, se procederá a instalar el reproductor multimedia VLC. Este software es libre y de código abierto desarrollado por el proyecto VideoLAN. Es un programa multiplataforma con versiones disponibles para muchos sistemas operativos; es capaz de reproducir casi cualquier formato de video sin necesidad de instalar codecs externos y puede reproducir videos en formatos DVD, Bluray, a resoluciones normales, en alta definición o incluso en ultra alta definición o 4K.

El primer paso para instalar una aplicación será buscarla. Para eso se debe presionar el botón "Buscar" y luego escribir, en este caso, "vlc" en la pantalla que se abre.

.. figure:: images/u2_debian_synaptic_vlc_01.png
    :align: center
    :scale: 60 %
    
En los resultados se debe identificar el paquete llamado"vlc":

.. figure:: images/u2_debian_synaptic_vlc_02.png
    :align: center
    :scale: 60 %
    
Al hacer clic derecho sobre el paquete, se verán las opciones disponibles. Se utilizará la opción "Marcar para instalar".

.. figure:: images/u2_debian_synaptic_vlc_03.png
    :align: center
    :scale: 60 %

Dicha opción resolverá automáticamente las dependencias e informará de las mismas, haciendo posible una correcta instalación. Hacer clic en "Marcar" para confirmar.

.. figure:: images/u2_debian_synaptic_vlc_04.png
    :align: center
    :scale: 60 %    

Luego de "Marcar" lo que se instalará, se deben aplicar todos los cambios seleccionados presionando el botón "Aplicar".
    
.. figure:: images/u2_debian_synaptic_vlc_05.png
    :align: center
    :scale: 60 %   

Se deberá confirmar esta acción presionando el botón "Aplicar" de la nueva pantalla.

.. figure:: images/u2_debian_synaptic_vlc_06.png
    :align: center
    :scale: 60 %   

Una vez que haya terminado de instalar los paquetes necesarios, nos informará que se aplicaron todos los cambios.

.. figure:: images/u2_debian_synaptic_vlc_07.png
    :align: center
    :scale: 60 %   

Se podrá comprobar que el reproductor VLC se encontrará en el menú de aplicaciones dentro de la categoría "Multimedia".

.. figure:: images/u2_debian_synaptic_vlc_08.png
    :align: center
    :scale: 60 %   

.. raw:: pdf

    PageBreak
    
Bibliografía
================================================================

El manual del Administrador de Debian. Debian Buster desde el descubrimiento a la maestría. Online: https://debian-handbook.info/browse/es-ES/stable/index.html.

Página oficial de VirtualBox. Online: https://www.virtualbox.org/

Página oficial de VLC. Online: https://www.videolan.org/vlc/


