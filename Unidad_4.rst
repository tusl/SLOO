================================================
Software Libre: Ofimática en las Organizaciones 
================================================
------------------------------------------------
Unidad 4: Personalización del Sistema
------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Unidad 4 - Personalización del Sistema

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022.

:Autor: Leopoldo Bertinetti


.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

.. contents:: Contenido

.. raw:: pdf

    PageBreak

    

Módulo 1: Personalización del Sistema
============================================================

En esta unidad, se verá la personalización de ciertos componentes de Debian Xfce. En otras palabras, se configurarán algunos aspectos visuales para darle un toque personal al sistema.

1. Reemplazar el Menú de aplicaciones por el menú Whisker
----------------------------------------------------------------------

El entorno Xfce posee un menú alternativo al que viene por defecto en Debian: el menú Whisker. A través de él se accede a programas y lugares de forma organizada. Basta con elegir la categoría deseada y encontrar el programa -o el lugar en el menú- para accionarlo y comenzar su utilización.

Para agregarlo, se debe hacer clic con el botón derecho del mouse sobre el panel y luego clic en "Añadir elementos nuevos...":

.. figure:: images/u4_agregar_menu1.png
    :align: center
    :scale: 60 %

Se abrirá una nueva ventana con distintos elementos que se podrán incorporar (también llamados **complementos**; este tema lo veremos con más detalles en una unidad posterior). Se debe buscar el *Menú Whisker* y luego hacer clic en "Agregar":

.. figure:: images/u4_agregar_menu2.png
    :align: center
    :scale: 60 %

Una vez añadido, aparecerá a la derecha del panel, por lo que se deberá mover hacia donde cada uno desee. Primeramente, se debe cerrar la ventana con los nuevos elementos.

.. figure:: images/u4_agregar_menu3.png
    :align: center
    :scale: 60 %

Para mover el menú agregado, se debe hacer clic con el botón derecho sobre él y luego clic en "Mover":

.. figure:: images/u4_agregar_menu4.png
    :align: center
    :scale: 60 %

En este caso, se movió a la izquierda del panel. Si hacemos clic sobre él, las categorías y aplicaciones de todo el sistema se muestran como en la siguiente imagen:

.. figure:: images/u4_agregar_menu5.png
    :align: center
    :scale: 60 %
    
Dado que se seguirá utilizando este menú, se podrá quitar el que venía por defecto, haciendo clic con el botón derecho sobre ese menú y luego en "Quitar":

.. figure:: images/u4_agregar_menu6.png
    :align: center
    :scale: 60 %

Se debe confirmar para quitarlo del panel:

.. figure:: images/u4_agregar_menu7.png
    :align: center
    :scale: 60 %

1.1 Personalizar el Menú Whisker
.....................................

Para acceder a la configuración del menú se puede hacer clic sobre él con el botón derecho del mouse y en el menú contextual que aparece, seleccionar la opción "Propiedades":

.. figure:: images/u4_menu1.png
    :align: center
    :scale: 60 %

Entre las opciones que se pueden modificar se destacan las relacionadas con la apariencia y el comportamiento del menú. Por ejemplo, se puede configurar para que aparezcan los nombres de las categorías, información emergentes de las aplicaciones y otras opciones más. Se recomienda probar la combinación de varias de las opciones que brinda.

.. figure:: images/u4_menu2.png
    :align: center
    :scale: 60 %

2. Personalizar los paneles
---------------------------------------------------------------------

Como se ha visto en unidades anteriores, el entorno de Debian Xfce viene, por defecto, con dos paneles, los cuales se pueden personalizar desde "Preferencias del panel":

.. figure:: images/u4_paneles1.png
    :align: center
    :scale: 60 %

En dichas preferencias, se podrán agregar más paneles o quitar alguno de los existentes.

.. figure:: images/u4_paneles2.png
    :align: center
    :scale: 60 %

En la pestaña "Presentación" se podrá configurar el modo del panel (horizontal / vertical), ocultarlo automáticamente, modificar sus medidas y otras opciones.

En "Apariencia" se podrá modificar el estilo del fondo del panel y la opacidad.

.. figure:: images/u4_paneles3.png
    :align: center
    :scale: 60 %
    

3. Personalizar el Escritorio
---------------------------------------------------------------------

Se debe ingresar a la aplicación "Escritorio". Se puede realizar de 2 maneras sencillas.

La primera será haciendo clic derecho sobre un espacio libre del Escritorio y eligiendo la opción "Configuración de escritorio"; y la segunda opción será buscando el acceso a través del menú Whisker, yendo a "Configuración" y luego eligiendo "Escritorio".

.. figure:: images/u4_escritorio1.png
    :align: center
    :scale: 60 %

.. figure:: images/u4_escritorio2.png
    :align: center
    :scale: 60 %
    
Se podrán personalizar 3 aspectos del escritorio:

* **Fondo:** entre las opciones más destacadas se puede mencionar la elección de distintos fondos de pantallas para las "Áreas de trabajo". Otra opción interesante es cambiar automáticamente el fondo cada cierto intervalo de tiempo.

.. figure:: images/u4_escritorio3.png
    :align: center
    :scale: 60 %

* **Menús:** se podrán configurar distintas opciones del menú desplegable, ya sea del menú del escritorio o del menú que se despliega al hacer clic derecho con el mouse sobre una ventana.

.. figure:: images/u4_escritorio4.png
    :align: center
    :scale: 60 %

* **Iconos:** se pueden configurar opciones sencillas sobre los íconos. Una opción interesante es mostrar las aplicaciones minimizadas como íconos.

.. figure:: images/u4_escritorio5.png
    :align: center
    :scale: 60 %


4. Personalizar la apariencia general
---------------------------------------------------------------------

Debian Xfce posee la herramienta gráfica llamada "Apariencia". Como su nombre indica, permite personalizar la apariencia del escritorio en general. Se puede encontrar en la categoría "Configuración" dentro del menú Whisker.

.. figure:: images/u4_apariencia1.png
    :align: center
    :scale: 60 %

Como se podrá observar, las opciones de personalización son varias: Estilo, Iconos, Tipos de letra y Configuración.

* **Estilo:** permite seleccionar un tema nuevo para cambiar la apariencia del contenido de la ventana.

.. figure:: images/u4_apariencia2.png
    :align: center
    :scale: 60 %

* **Iconos:** brinda la posibilidad de elegir un tema para cambiar la apariencia de los íconos.

.. figure:: images/u4_apariencia3.png
    :align: center
    :scale: 60 %

* **Tipos de letra:** permitirá configurar la letra y el tamaño, entre otras cuestiones.

.. figure:: images/u4_apariencia4.png
    :align: center
    :scale: 60 %

* **Configuración:** en esta pestaña se podrá configurar algunos aspectos de los menús y botones y si se desea activar sonidos de eventos.

.. figure:: images/u4_apariencia5.png
    :align: center
    :scale: 60 %


5. Personalizar las ventanas
---------------------------------------------------------------------

En este punto, se mostrarán las opciones de personalización de las ventanas. Se utilizará la herramienta gráfica "Gestor de ventanas", accediendo desde el menú Whisker. 

.. figure:: images/u4_ventanas1.png
    :align: center
    :scale: 60 %

Desde la herramienta, se podrá cambiar fácilmente el Tema (estilo de ventanas), el tipo y tamaño de las tipografías de los títulos, la alineación del título y la distribución de los botones de la ventana.

.. figure:: images/u4_ventanas2.png
    :align: center
    :scale: 60 %

Se observa que existen más pestañas para personalizar otros aspectos, destacándose la pestaña "Teclado", en la cual se podrán configurar los atajos para realizar acciones del gestor de ventanas.

.. figure:: images/u4_ventanas3.png
    :align: center
    :scale: 60 %

    
6. Administrador de Configuración
---------------------------------------------------------------------

Por último, se nombra la herramienta gráfica "Administrador de Configuración", en la cual se podrán encontrar las herramientas ya mencionadas y otras que pueden llegar a interesar. 

Se accede desde el menú Whisker, presionando el botón "Toda la configuración".

.. figure:: images/u4_administrador_conf.png
    :align: center
    :scale: 60 %

.. figure:: images/u4_administrador_conf1.png
    :align: center
    :scale: 60 %
