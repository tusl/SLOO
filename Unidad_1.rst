================================================
Software Libre: Ofimática en las Organizaciones 
================================================
------------------------------------------------
Unidad 1: Introducción a GNU/Linux
------------------------------------------------

.. header:: 
  Software Libre: Ofimática en las Organizaciones - Unidad 1 - Introducción a GNU/Linux

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022.

:Autor: Leopoldo Bertinetti        

.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

.. contents:: Contenido

.. raw:: pdf

    PageBreak



Módulo 1: Comenzando con GNU/Linux - Debian
=============================================

Como se mencionó en la presentación de la materia, el enfoque de la misma es esencialmente práctico. Sin embargo, antes de comenzar, resulta conveniente revisar algunas ideas que ayudarán a organizar el tema, ideas que luego se irán profundizando además de incorporar otros conceptos.

GNU es un proyecto que tiene como objetivo crear un sistema operativo completamente libre de tipo Unix, lo que significa que se trata de una colección de muchos programas: aplicaciones, bibliotecas, herramientas de desarrollo y hasta juegos. El nombre GNU es un acrónimo recursivo de "GNU No es Unix".

En un sistema de tipo Unix, el programa que asigna los recursos de la máquina y se comunica con el hardware se denomina "núcleo". GNU se usa generalmente con un núcleo llamado "Linux" y esta combinación es el sistema operativo GNU/Linux.

Las distintas formas de organizar las aplicaciones mencionadas, en base a ciertos principios, es lo que dá origen a las denominadas "distribuciones" de GNU/Linux como Debian, RedHat, CentOS, Ubuntu, y muchísimas otras más. Algunas distribuciones se basan sobre otras (por ejemplo Ubuntu sobre Debian) y otras son derivadas (por ejemplo, CentOS de RedHat). Más adelante, se verán con mayor profundidad.

Debian es un sistema operativo estable y seguro basado en Linux. Está hecho de software libre y de código abierto, por lo que todo el mundo es libre de usarlo, modificarlo y distribuirlo. Esa es su principal premisa.

Por otra parte, es adecuado para un amplio rango de dispositivos incluyendo computadoras portátiles, de escritorio y servidores.

También se debe destacar que Debian no es solamente un sistema operativo Linux, sino que es una comunidad en la cual el software se desarrolla mediante la colaboración de cientos de voluntarios de todo el mundo.

Éstas son algunas de las razones por las cuales se escogió Debian para iniciar con la introducción a este mundo del software libre.

El nombre Debian se debe a la combinación de los nombres de su creador, Ian Murdock, y de su esposa, Debra. Véase, *"Una breve historia de Debian"* [#]_.

.. [#] Una breve historia de Debian: https://www.debian.org/doc/manuals/project-history/

Se hace necesario resaltar que Debian, según la Free Software Foundation, no es un sistema operativo completamente libre. Véase, *"Por qué no avalamos otros sistemas"* [#]_. 

.. [#] Sistemas GNU libres: http://www.gnu.org/distros/common-distros.es.html

Por último, se debe aclarar que en esta materia se utilizará Debian con el entorno de escritorio *Xfce*, cuyo objetivo es ser rápido y consumir pocos recursos del sistema. En unidades posteriores de la materia, se retomará el tema de los entornos de escritorio.

La versión "estable" actual de Debian es la 11, cuyo nombre clave es "Bullseye". Cada versión tiene un soporte completo por 3 años y se les suman 2 años de soporte extra (LTS, del inglés, *Long Time Support*), es decir, que durante los cinco años siguientes a la liberación de dicha versión estarán disponibles actualizaciones importantes y de seguridad para el sistema.


1.1 Requerimientos del sistema
--------------------------------

**Requerimientos mínimos del sistema:** Para instalar o probar Debian con un entorno gráfico (Xfce), se necesita contar con al menos 1 GB de memoria RAM y 2 GB de espacio libre en el disco duro.

**Requerimientos recomendados del sistema:** Para obtener una buena experiencia al ejecutar aplicaciones en el escritorio, se recomienda tener al menos 2 GB de memoria RAM y unos 10 GB de disco duro. Esto permite la instalación de nuevas aplicaciones y el almacenamiento de datos personales en el disco duro, además del sistema central. 


1.2 Descargas de Debian 11
-------------------------------------

A partir de la versión 10, Debian se puede probar arracando el sistema "en vivo" desde un CD, DVD o USD sin necesidad de instalar ningún archivo en la computadora. Luego de probarlo, se puede decidir si se ejecuta el instalador que viene incluido, llamado "Calamares", que es muy fácil de usar por el usuario final. Véase, *"Imágenes de instalación en vivo"* [#]_.

.. [#] Imágenes de instalación en vivo: https://www.debian.org/CD/live/#choose_live

A continuación se listan los enlaces para descargar Debian con el método anterior.

Dependiendo de la calidad y velocidad de la conexión a Internet, la descarga se puede realizar a través del archivo .torrent (previamente se requiere instalar un cliente) o descargando directamente el archivo .ISO [#]_.

.. [#] Imagen ISO: https://es.wikipedia.org/wiki/Imagen_ISO

Cualquiera sea el caso, en esta materia se utilizará la versión de **32 bits** (arquitectura i386).

Se sugiere utilizar alguno de los siguientes enlaces:

**TORRENT:** https://cdimage.debian.org/debian-cd/current-live/i386/bt-hybrid/debian-live-11.3.0-i386-xfce.iso.torrent

**DIRECTA:** https://cdimage.debian.org/debian-cd/current-live/i386/iso-hybrid/debian-live-11.3.0-i386-xfce.iso

Sin importar la forma en la que se descarga el archivo de instalación, al final se obtendrá un archivo con formato ISO. Este formato permite grabar la distribución directamente en un CD/DVD/USB, con lo cual, como ya se mencionó, se podrá ejecutar el sistema operativo y todas sus aplicaciones incluidas, sin necesidad de instalar nada adicional en el disco rígido. 

Por lo expuesto, es una buena manera de comenzar a ver y probar GNU/Linux. Sin embargo, aquí no se indicará la grabación de la ISO en un CD/DVD/USB, sino que se utilizará una herramienta que emulará lo comentado anteriormente: VirtualBox.

Módulo 2: VirtualBox
=====================

Esta herramienta servirá para realizar pruebas de instalaciones de cualquier distribución de GNU/Linux.

VirtualBox es un software que posibilita la creación de máquinas virtuales, a las cuales se les asignan recursos de sistemas virtuales: memoria RAM, disco rígido, placas de red y otros. No se pretende profundizar en conocimientos de virtualización sino contar con un ambiente de pruebas que facilite la instalación de distintos sistemas operativos GNU/Linux. 

Para ampliar información, se puede descargar el Manuel de Usuario de VirtualBox desde su página oficial (disponible en http://download.virtualbox.org/virtualbox/UserManual.pdf).

Para instalar VirtualBox:

* En Windows: dirigirse a la siguiente URL: https://www.virtualbox.org/wiki/Downloads. La versión actual se puede descargar donde dice "VirtualBox 6.1.34 platform packages" / "Windows hosts". Luego se debe ejecutar el instalador (archivo .exe) y continuar con el proceso de instalación.

* En Linux: se puede instalar VirtualBox directamente a través del gestor de paquetes que tenga disponible en su distribución.


2.1 Creación de máquinas virtuales (VMs)
-----------------------------------------

Luego de iniciar VirtualBox, se debe seleccionar la opción "Nueva" para crear una nueva máquina virtual o VM (del inglés, "Virtual Machine"):

.. figure:: images/u1_vbox1.png
    :align: center
    :scale: 54 %

El siguiente paso es colocar nombre a la VM y especificar el tipo de sistema operativo. En *Tipo* seleccionar "Linux" y en *Versión*, "Debian (32-bit)":

.. figure:: images/u1_vbox2.png
    :align: center
    :scale: 54 %

Luego se especifica la memoria RAM que se le asignará a la VM. Esta cantidad está limitada por la memoria disponible en su sistema (si es posible, asignar 2 GB o 2048 MB, ya que es lo recomendado en los requirimientos, veáse punto 1.1):

.. figure:: images/u1_vbox3.png
    :align: center
    :scale: 54 %

En la pantalla siguiente, por ahora, se selecciona la opción "No agregar un disco duro virtual". Luego, hacer clic en "Crear":

.. figure:: images/u1_vbox4.png
    :align: center
    :scale: 54 %

Aparecerá un cartel que advierte que se creará una máquina virtual sin disco. Clic en "Continuar":

.. figure:: images/u1_vbox5.png
    :align: center
    :scale: 54 %

2.2 Configuración de imagen ISO como disco de inicio en VM
-----------------------------------------------------------

En este momento ya se tiene creada una VM. A continuación, a esta VM se le asignará la imagen ISO de Debian descargada anteriormente, simulando que se está introduciendo el CD/DVD de instalación de Debian en la lectora de CD/DVD. Para realizar esto, se selecciona la VM creada y se visualiza la "Configuración". 

.. figure:: images/u1_vbox6_1.png
   :align: center
   :scale: 54 %

Luego, en la sección "Almacenamiento", se agrega un nuevo dispositivo CD/DVD (unidad óptica) como se muestra a continuación:

.. figure:: images/u1_vbox6.png
   :align: center
   :scale: 54 %

Aparece una ventana en la cual al presionar el botón "Añadir", se puede seleccionar la imagen ISO descargada.

.. figure:: images/u1_vbox7.png
    :align: center
    :scale: 54 %

.. figure:: images/u1_vbox7_2.png
    :align: center
    :scale: 54 %

La imagen seleccionada debería quedar reflejada como un nuevo dispositivo:

.. figure:: images/u1_vbox7_3.png
    :align: center
    :scale: 54 %
    
Lo siguiente es configurar la VM para que inicie desde la unidad de CD/DVD, que en realidad iniciará desde la imagen ISO que fue previamente asignada. Para eso, se debe ir a la sección "Sistema" y en "Orden de arranque" cambiar las opciones para que "Óptica" quede en el primer lugar, como se muestra a continuación:

.. figure:: images/u1_vbox8.png
    :align: center
    :scale: 54 %
    
Para guardar todos los cambios realizados, se debe hacer clic en el botón "Aceptar".

Finalmente, se inicia la VM:

.. figure:: images/u1_vbox9.png
    :align: center
    :scale: 54 %

La primera de las opciones es la que permite probar Debian sin instalar ningún archivo.

.. figure:: images/u1_vbox10.png
    :align: center
    :scale: 54 %

El procedimiento seguido permite crear otras VMs y asignarles imágenes ISO de todas las distribuciones que se desean probar.


Módulo 3: El entorno de trabajo en Debian (Xfce) 
=================================================

Cuando termina de iniciarse Debian, aparece la siguiente pantalla:

.. figure:: images/u1_debian01.png
    :align: center
    :scale: 55 %

Lo que se observa es el escritorio de Debian. **Es importante señalar que al ser una versión para probarlo "en vivo", no viene con los paquetes de idiomas instalados, por lo que se podrá probar sólo en el idioma inglés**. Sin embargo, si posteriormente se decide instalarlo, se podrá seleccionar el idioma del sistema.

A continuación, se analizará cada uno de sus componentes.


3.1 El Escritorio
------------------


El Escritorio es la aplicación que posibilita el acceso a las herramientas a través de sus menús o íconos, además de alternar entre las diferentes aplicaciones abiertas que se están ejecutando paralelamente.

El escritorio predeterminado incluye tres íconos: *Home (Carpeta personal)*, *File System (Sistema de archivos)* y *Trash (Papelera)*. Los íconos para unidades extraíbles (CD o memorias USB), aparecen sólo si están presentes. En esta ocasión, al estar utilizando Debian "en vivo" también aparece el ícono del instalador (*Install Debian*).

Se puede personalizar el escritorio haciendo clic derecho en una zona de la imagen de fondo y eligiendo *Desktop Settings (Configuraciones de escritorio)*. También es posible cambiar el fondo de escritorio, modificar el menú y el comportamiento de los íconos.

.. figure:: images/u1_debian02.png
    :align: center
    :scale: 55 %


3.2 Paneles
------------

En la parte superior e inferior del escritorio, se pueden observar dos barras o paneles.

Al hacer clic con el botón derecho del mouse sobre cada panel, se puede acceder a sus configuraciones (*Panel -> Panel Preferences*):

.. figure:: images/u1_debian_panel1.png
    :align: center
    :scale: 55 %

El panel superior ("Panel 1") se utiliza para iniciar e intercambiar aplicaciones, así como para recibir información del sistema.

.. figure:: images/u1_debian_panel1_1.png
    :align: center
    :scale: 55 %

Se puede observar que entre sus elementos (o "Items") aparece el "Menú de aplicaciones" (*Applications Menu*), la sección dedicada a los "Botones de Ventanas" (*Window Buttons*; cuando hay aplicaciones "abiertas"), las "Áreas de Trabajo" (*Workspace Switchers*), el "Área de Notificación" (con plugins de diferentes aplicaciones), el "Reloj" (*Clock*) y "Botones de Acción" (*Action Buttons*).

Cada uno de esos elementos, también puede ser configurado con la opción "Edit the currently selected item":

.. figure:: images/u1_debian_panel1_2.png
    :align: center
    :scale: 55 %

El panel inferior ("Panel 2") muestra una lista de lanzadores (*Launcher*) de algunas aplicaciones. Como en el caso anterior, se puede configurar:

.. figure:: images/u1_debian_panel2.png
    :align: center
    :scale: 55 %

También se pueden agregar otros elementos a los paneles al hacer clic con el botón derecho del mouse (*Panel -> Add New Items*):

.. figure:: images/u1_debian_paneles.png
    :align: center
    :scale: 55 %
    
.. figure:: images/u1_debian_paneles2.png
    :align: center
    :scale: 55 %

3.2.1 Menú de aplicaciones
............................

Es el menú principal de Debian; a través de él se accede a programas y lugares de forma organizada. Basta con elegir la categoría deseada y encontrar el programa -o el lugar en el menú- para accionarlo y comenzar su utilización.

.. figure:: images/u1_debian_appmenu2.png
    :align: center
    :scale: 55 %

Para acceder a la configuración del menú se puede hacer clic sobre él con el botón derecho del mouse y en el menú contextual que aparece, seleccionar la opción "Properties (Propiedades)":
    
.. figure:: images/u1_debian_appmenu3.png
    :align: center
    :scale: 55 %    
    
.. figure:: images/u1_debian_appmenu.png
    :align: center
    :scale: 55 %    

Por otra parte, es importante remarcar la opción que aparece al final del menú para cerrar la sesión (*Log Out*).

.. figure:: images/u1_debian_logout1.png
    :align: center
    :scale: 55 %   

.. figure:: images/u1_debian_logout2.png
    :align: center
    :scale: 55 %   
    
Existen varias posibilidades al cerrar la sesión del usuario:

- **Log Out (Cerrar sesión)**: se utiliza para permitir que otros usuarios usen su equipo. Todas las aplicaciones abiertas en su sesión se cerrarán.

- **Restart (Reiniciar)** o **Shut Down (Apagar)**: permiten reiniciar o apagar el sistema, respectivamente.

- **Suspend (Suspender)**: permite guardar el trabajo que se está realizando, las aplicaciones, documentos abiertos y la configuración en la memoria del equipo. Entra en un estado de bajo consumo energético en el cual se apaga todo, pero necesita en todo momento un mínimo de corriente eléctrica para posteriormente recuperar esa memoria.
    
    En este punto, se hace necesario remarcar la diferencia con la **hibernación**, la cual permite guardar el trabajo y la configuración en el *Disco Duro* del equipo. Es un estado donde el equipo no consume nada de energía. El estado de hibernación es similar al de Suspender, pero guarda una copia de la memoria en el Disco Duro, de modo que no necesita ninguna energía. Se apaga todo y no es necesario un aporte mínimo de corriente para restablecerse al estado normal.

- **Switch User**: se utiliza para cambiar de usuario en el equipo, pero sin cerrar las aplicaciones abiertas en la sesión.

3.2.2 Botones de las ventanas
..............................


Si bien ya se ha nombrado la palabra *ventana*, no se ha definido su concepto. Una ventana es el espacio en el Escritorio que ocupa cualquier aplicación que se abre. Cada aplicación posee su ventana y podemos utilizar varias al mismo tiempo.

Una ventana posee una barra de título superior con el nombre de la aplicación que está siendo ejecutada y tres botones funcionales del lado derecho: Minimizar (-), Restaurar/Maximizar (+) y Cerrar (x).

En la siguiente imagen, a modo de ejemplo, se muestra la ventana para la aplicación Terminal:

.. figure:: images/u1_debian_ventana.png
    :align: center
    :scale: 55 %

Debe notarse que en la sección llamada "Botones de las ventanas" del panel superior se muestra un botón que representa la aplicación abierta. Si la ventana es minimizada, la aplicación quedará representada únicamente por dicho botón del panel. Luego, al hacer click en el botón se mostrará nuevamente la ventana.

Al cerrar la aplicación, la representación de ésta como una ventana en el Escritorio y en el Panel desaparece.

3.2.3 Área de notificación
............................

Como se ha mencionado anteriormente, en el Área de Notificación se muestran indicadores que brindan información del sistema. En la siguiente figura, se resalta con rojo la ubicación de esta área:

.. figure:: images/u1_debian_notification.png
    :align: center
    :scale: 55 %
    
En el ejemplo se muestran los indicadores del *idioma del teclado*, de la *red*, del *volumen*, de la *energía* y de las notificaciones generales.

3.2.4 Reloj
............

Al hacer click derecho sobre él, se abre un menú contextual con la opción "Properties (Propiedades)", que permite configurar la hora y la zona horaria, cambiar la apariencia y el formato del reloj.

.. figure:: images/u1_debian_reloj.png
    :align: center
    :scale: 55 %


3.3 Íconos
------------

Un ícono es una representación gráfica de una aplicación o de un documento de la Pc. Está compuesto por una pequeña figura que simboliza la aplicación o el documento y por una leyenda que contiene el nombre de la aplicación o del documento propiamente dicho. 

Por ejemplo, el escritorio incluye tres íconos: *Carpeta personal* (*Home*), *Sistema de archivos* (*File System*) y *Papelera* (*Trash*).


3.4 Administración de archivos
-------------------------------

3.4.1 Sistema de archivos
..........................

La información almacenada en las computadoras está estructurada en un sistema de archivos y directorios. Por convención, esta estructura se representa como un árbol; un árbol de directorios. En realidad, se trata de un árbol invertido ya que la raíz está arriba y las ramas se expanden hacia abajo.

El sistema de archivos de Linux tiene un origen único: la raíz o root representada por /. Bajo este directorio se encuentran todos los ficheros a los que puede acceder el sistema operativo. Estos ficheros se organizan en distintos directorios; cada uno de ellos tiene una misión y un nombre estándar, dados para todos los sistemas Linux:

- **/:** Raíz del sistema de archivos.

- **/bin:** Archivos binarios de las aplicaciones.

- **/dev:** Ficheros del sistema representando los dispositivos que están físicamente instalados en el ordenador.

- **/etc:** Directorio reservado para los ficheros de configuración del sistema.

- **/home:** Directorios personales de cada usuario.

- **/lib:** Librerías necesarias para que se ejecuten los programas que residen en /bin.

- **/media:** Lugar donde se montan las unidades removibles, tales como pendrives, CDs/DVDs, cámaras digitales y otras.

- **/proc:** Ficheros especiales que reciben o envían información al kernel del sistema.

- **/sbin:** Programas únicamente accesibles para el superusuario o root.

- **/sys:** Lugar de guardado de los archivos del sistema.

- **/usr:** Uno de los directorios más importantes del sistema puesto que contiene los programas de uso común para todos los usuarios.

- **/var:** Directorio destinado a la información temporal de los programas.


3.4.2 Gestor de archivos
..........................

Para navegar en el sistema de archivos anterior, Debian Xfce incluye un gestor de archivos denominado *Thunar*. Para acceder a él, hay que dirigirse al menú de aplicaciones y seleccionar "Thunar File Manager" de la categoría "Accesorios". También se puede hacer clic sobre el ícono "File System" que aparece en el escritorio.

Consta de un panel de atajos en la parte izquierda, el área principal a la derecha y una barra de ruta encima. El panel incluye atajos a diferentes carpetas del sistema:

- **DEVICES (Dispositivos):** Aquí aparece el disco duro. Al abrirlo, se mostrará el sistema de archivos desde la raíz. Además, en esta sección se montarán los dispositivos que se conectan a la Pc (pendrives, memorias USB, CDs y otros).

- **PLACES (Ubicaciones):** Se encuentran atajos a la carpeta personal, al Escritorio (Desktop) y a la Papelera.

- **NETWORK (Redes):** Si la Pc está conectada a una red de área local, aquí se pueden buscar carpetas y archivos compartidos de otras Pcs.

El área principal siempre muestra el contenido de la carpeta actual. Al hacer doble click en las carpetas se acceden a ellas, y al hacer click derecho en los archivos o carpetas, aparece una ventana emergente con varias opciones.

La barra de ruta muestra la ubicación completa (desde la raíz) de la carpeta que se está visualizando en el área principal. Por ejemplo, si se está mostrando el contenido de la carpeta personal, en la barra se leerá: /home/user ("user" es el nombre del usuario que viene por defecto cuando se inicia el Live CD/DVD).

.. figure:: images/u1_debian_thunar.png
    :align: center
    :scale: 55 %


3.4.3 Carpeta personal
.......................

Al hacer click sobre el ícono "Carpeta personal" ubicado en el Escritorio, aparece la vista de la carpeta con su contenido.

Este espacio está destinado para uso exclusivo y está identificado por el nombre del usuario. En este caso, al estar probando Debian "en vivo", aparece con el nombre "user". Más adelante, se verá que si el usuario se llama "usuario1", la carpeta personal estará identificada con "usuario1" y en ella se guardarán los archivos propios.

Si la computadora es apagada y otra persona la enciende, entrará con su propio usuario. Este otro usuario tendrá, también, un ícono llamado "Carpeta personal" en su menú "Ubicaciones", que apuntará a su cuenta.

.. figure:: images/u1_debian_home.png
    :align: center
    :scale: 55 %


.. raw:: pdf

   PageBreak     
    

Bibliografía 
=============

Página oficial de la Free Software Foundation (FSF). Online: https://fsf.org/.

Alfabetización Digital Básica. Proyecto conectar libertad. Daniel Armando Rodríguez. Posadas, Misiones, Argentina. Abril 2011.

Guía de instalación de Debian GNU/Linux. Online: https://www.debian.org/releases/stable/i386/.

Aprenda LINUX como si estuviera en primero. Javier García de Jalón, Iker Aguinaga y Alberto Mora. Universidad de Navarra. Enero 2000.

"Nuestra filosofía: por qué lo hacemos y cómo lo hacemos". Online: https://www.debian.org/intro/philosophy

"Razones principales". Online: https://www.debian.org/intro/why_debian
