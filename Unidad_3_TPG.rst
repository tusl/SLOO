================================================
Software Libre: Ofimática en las Organizaciones 
================================================
--------------------------------------------------------------------
Trabajo Práctico Grupal - Distribuciones GNU/Linux
--------------------------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Trabajo Práctico Grupal

.. footer::
  Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022

:Autor: Leopoldo Bertinetti


.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

Trabajo Práctico Grupal - Distribuciones GNU/Linux
===================================================

Fecha de entrega: 25/07/2022
---------------------------------------------------

Pautas generales
----------------------------

El trabajo práctico se realizará de manera colaborativa de acuerdo a los grupos conformados por el docente. Cada grupo realizará un informe sobre una distribución GNU/Linux asignada por el docente.

El formato del archivo debe ser abierto (se recomienda ODT o PDF).

El nombre del archivo debe tener el siguiente formato: TPG_GrupoX.pdf o TPG_GrupoX.odt.

Sólo uno de los integrantes del grupo deberá subir el archivo del Trabajo Práctico a la tarea correspondiente dentro del aula ("Subir archivo Trabajo Práctico Grupal").

Se recomienda leer *"Primeros pasos con Writer"* para configurar el archivo según el formato que se solicita:

https://documentation.libreoffice.org/assets/Uploads/Documentation/es/GS62/PDF/GS6204-PrimerosPasosConWriter.pdf


Formato
------------

**Formato de papel:** A4

**Márgenes:**

- Superior: 3,0 cm.
- Inferior: 2,0 cm.
- Izquierdo: 3,0 cm.
- Derecho: 2,0 cm.

**Carátula:** Texto centrado, fuente Arial, sin numerar. Debe contener:

- Rótulo identificatorio de la Unidad Académica y la materia (tamaño 14pts): UNLVirtual - Facultad de Ingeniería y Ciencias Hídricas - Tecnicatura Universitaria en Software Libre - Software Libre: Ofimática en las Organizaciones.

- Título del Trabajo Práctico (iniciales en mayúsculas, negritas y tamaño 16pts): Trabajo Práctico Grupal - distribución que le corresponde al grupo.

- Nombre y apellido de los alumnos (iniciales en mayúsculas, tamaño 14pts).

- Año de cursado (tamaño 12pts).

- Seleccionar una licencia Creative Commons (http://www.creativecommons.org.ar/licencias). **Ayuda:** https://creativecommons.org/choose/?lang=es_AR

A continuación se puede apreciar un modelo de la carátula:

.. figure:: images/u3_caratula_tpg.png
    :align: center
    :scale: 65 %

**Cada hoja del TPG:** contiene:

- Cabecera de página o encabezado: Título del Trabajo Práctico y materia (iniciales en mayúsculas).

- Pie de página: Nombre y apellido de los alumnos, separados por un guión medio (iniciales en mayúsculas). Además, en la esquina inferior derecha se debe introducir el número de página.

**Títulos:** Texto alineado a la izquierda, en negritas, fuente Arial y tamaño 14pts, con una separación del párrafo superior de 0,4 cm y del párrafo inferior de 0,4 cm.

**Subtítulos:** Texto alineado a la izquierda, en negritas y cursivas, fuente Arial y tamaño 12pts, con una separación del párrafo superior de 0,4 cm y del párrafo inferior de 0,4 cm.

**Párrafos:** Texto justificado, fuente Arial, tamaño 10pts, interlineado sencillo, con una sangría de 1,0 cm (en el primer renglón) y con una separación del párrafo superior de 0,2 cm y del párrafo inferior de 0,2 cm.



.. raw:: pdf

    PageBreak

Criterios de evaluación:
---------------------------

Se examinará:

    - Si se respetan las pautas enunciadas.
    - Si en el informe se responden todas las consignas solicitadas.
    - Si se respeta a los autores (citar en el caso de ser necesario).

    
Enunciado:
-----------
    
Se debe realizar en grupo un informe sobre la distribución que se le asignó, donde se incluyan los siguientes puntos:
 
    #. Breve descripción de la distribución.
    #. Breve historia (incluir país y año de surgimiento).
    #. Motivos de su creación (intenciones de los creadores).
    #. Tipos de usuarios a la que está orientada.
    #. Responsables de su mantenimiento y/o soporte.
    #. Categorización según sus libertades (Ayuda: puede utilizar el comando *vrms* para chequear cuánto software privativo posee la distribución).
    #. Categorización según sus tipos de paquetes (incluir gestores de paquetes).  
    #. Comunidades de Software Libre detrás de la distribución. ¿Qué rol cumplen? ¿Cómo contribuyen a la distribución?
    #. Conclusión grupal (aspectos positivos y negativos).



.. raw:: pdf

    Spacer 0 50

:Aclaración: Es indispensable que el Trabajo Práctico tenga elaboración propia. Evitar la copia textual de internet. Las conclusiones deben ser realizadas por el grupo, no por un sólo integrante.
