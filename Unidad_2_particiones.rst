================================================
Software Libre: Ofimática en las Organizaciones 
================================================
------------------------------------------------
Unidad 2: Resumen de Particiones
------------------------------------------------

.. header::
  Software Libre: Ofimática en las Organizaciones - Unidad 2 - Resumen de Particiones

.. footer::
  Tecnicatura Universitaria en Software Libre - FICH-UNL - Página ###Page### de ###Total###

.. raw:: pdf

    Spacer 0 200

Copyright©2022.

:Autor: Leopoldo Bertinetti


.. rubric:: ¡Copia este texto!

Los textos que componen este libro se publican bajo formas de licenciamiento que permiten la copia, la redistribución y la realización de obras derivadas siempre y cuando éstas se distribuyan bajo las mismas licencias libres y se cite la fuente.
El copyright de los textos individuales corresponde a los respectivos autores.
`El presente trabajo está licenciado bajo un esquema Creative Commons Atribución CompartirIgual (CC-BY-SA) 4.0 Internacional. <http://creativecommons.org/licenses/by-sa/4.0/deed.es>`_

.. image:: images/licencia.png

.. raw:: pdf

    PageBreak

.. contents:: Contenido

.. raw:: pdf

    PageBreak





1. Introducción
============================================================

En este apunte encontrarán un resumen acerca de las particiones de los discos, tema sumamente importante a la hora de instalar un sistema operativo.
Se empezará definiendo lo que es una partición para luego ir analizando los demás conceptos que se desprenden de ella.


2. Concepto de partición
============================================================

Una partición de disco es el nombre que recibe cada división presente en una sola unidad física de almacenamiento de datos (disco duro). Cada partición se puede acceder como si fuera un disco aparte. Esto se hace por medio de una tabla de particiones (ver sección *"4. Esquemas de particiones (tablas de particiones)"*).
Además, toda partición tiene su propio sistema de archivos que define su formato (ver sección *"3. Sistemas de archivos (formato de particiones)"*).

Hay varias razones para asignar espacio de disco en particiones de disco separadas. Algunos ejemplos son:

* Separación lógica entre los datos del sistema operativo y los datos del usuario.
* Capacidad para usar diferentes sistemas de archivos.        
* Capacidad de ejecutar múltiples sistemas operativos en una máquina.
        


3. Sistemas de archivos (formato de particiones)
============================================================

El sistema de archivos es el componente del sistema operativo encargado de asignar espacio de disco a los archivos, de administrar el espacio libre y de acceder a los datos guardados. En otras palabras, organiza los archivos en el disco.
Es importante remarcar que la mayoría de los sistemas operativos manejan su propio sistema de archivos por lo que tienden a ser incompatibles. Sin embargo, Linux es compatible con una gran variedad de sistemas de archivos, lo que facilita el intercambio de datos entre distintos sistemas de archivos.

A continuación se mencionan algunos de los formatos más utilizados:


+---------------------------------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Sistema de archivos**                     | **Sistema operativo**                      | **Descripción**                                                                                                                                                                         |
+=============================================+============================================+=========================================================================================================================================================================================+
| FAT32, exFAT, NTFS                          | Windows                                    | Sistemas usados por Windows. GNU/Linux puede trabajar con ellos. macOS puede leer las unidades formateadas con estos sistemas, pero no puede escribir en ellas, con excepción de FAT32. |
+---------------------------------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| HFS+                                        | macOS                                      | Sistema creado por Apple. Los sistemas GNU/Linux pueden trabajar con él. Con Windows sólo se puede leer el contenido de los discos formateados con este sistema.                        |
+---------------------------------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ext2, ext3, ext4                            | GNU/Linux                                  | Utilizados sólo por las distribuciones GNU/Linux.                                                                                                                                       |
+---------------------------------------------+--------------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+



4. Esquemas de particiones (tablas de particiones)
============================================================

En la sección anterior, se mencionaron los formatos que pueden tener las particiones, es decir, cómo se guardan los archivos en el disco, pero ¿cómo se hace para acceder a cada partición? La respuesta está en la tabla de particiones, donde se indica el inicio y el fin de cada partición, entre otra información.

Actualmente existen dos estándares de distribución de particiones para los discos duros físicos: el *registro de arranque maestro* (MBR, del inglés Master Boot Record) y la *tabla de particiones GUID* (GPT, del inglés GUID Partition Table).


4.1 MBR (Master Boot Record) 
---------------------------------------------------------------------


Es el estándar de partición más antiguo pero sigue siendo totalmente funcional. Es un método para realizar particiones de disco que se utiliza con equipos basados en BIOS (ver sección *"6. Modos de arranque (BIOS y UEFI)"*). Su limitación más importante es que sólo soporta discos duros de hasta 2 TB. Otra limitación es que sólo puede trabajar con 4 particiones primarias, por lo que para crear más de 4 debemos recurrir a las particiones extendidas (ver sección *"5. Tipos de particiones"*).


4.2 GPT (GUID Partition Table)
---------------------------------------------------------------------
La tabla de particiones GUID es un nuevo esquema de partición que se basa en el uso de identificadores únicos globales (GUID). Es el nuevo estándar que está sustituyendo a MBR. GPT es una distribución de particiones que forma parte de la Interfaz de Firmware Extensible Unificada (UEFI) (ver sección *"6. Modos de arranque (BIOS y UEFI)"*). GPT elimina las principales limitaciones de MBR, dado que permite hasta 128 particiones primarias en un solo disco y es compatible con discos duros de gran tamaño (mayores a 2 TB).

Por otra parte, los discos con GPT son más fiables que los discos con MBR. Ésta última tabla de particiones se almacena sólo en los primeros sectores del disco, por lo que al perderse, corromperse o sobre-escriturarse, se pierde toda la información del disco. GPT, en cambio, crea múltiples copias redundantes a lo largo de todo el disco de manera que, en caso de fallo, problema o error, la tabla de particiones se recupera automáticamente desde cualquiera de dichas copias.

A continuación, se muestra una imagen donde se compara un disco con MBR y otro con GPT:

.. figure:: images/u2_comparativa-MBR-GPT.png
    :align: center
    :scale: 75 %


5. Tipos de particiones
============================================================


5.1 Particiones primarias
---------------------------------------------------------------------

Una partición primaria es una partición en el disco duro que solo puede contener una unidad (o sección) lógica.

La tabla de particiones contenida en el MBR se divide en cuatro secciones. Cada sección puede incluir la información necesaria para definir una única partición, lo que significa que esta tabla de particiones no puede definir más de cuatro particiones.

    
5.2 Particiones extendidas
---------------------------------------------------------------------

Si se utiliza el esquema MBR y las cuatro particiones no bastan para cubrir las necesidades, se pueden emplear particiones extendidas para crear más particiones. Una partición extendida es como una unidad de disco por derecho propio — posee su propia tabla de particiones que remite a una o más particiones (ahora llamadas *particiones lógicas*, en contraposición a las cuatro particiones primarias) contenidas en su totalidad en la propia partición extendida.

Notar que sólo pueden existir cuatro particiones primarias, sin embargo, no hay ningún límite para el número de particiones lógicas que pueden existir. También es necesario tener en cuenta que sólo las particiones primarias y lógicas pueden contener un sistema de archivos propio. Las particiones extendidas carecen de esta característica porque fueron hechas sólo para contener otras particiones.

A continuación, se muestra una imagen donde se representa un disco particionado:


.. figure:: images/u2_particionado.png
    :align: center
    :scale: 66 %

    
6. Modos de arranque (BIOS y UEFI)
============================================================

En esta sección, se verán los dos firmware que se utilizan en la actualidad y que están estrechamente vinculados con los esquemas de particiones, ya que la BIOS se utiliza bajo el esquema MBR, mientras que la GPT forma parte de la UEFI.

Tanto BIOS como UEFI son firmware, es decir, una porción de código que está almacenada en una memoria situada en la placa madre de la computadora y contiene las instrucciones que controlan las operaciones de los circuitos de la computadora. Entonces se encargan de iniciar, configurar y comprobar que se encuentre en buen estado todo el hardware de la computadora, incluyendo la memoria RAM, los discos duros, la placa base o la tarjeta gráfica. Cuando terminan, seleccionan el dispositivo de arranque (disco duro, CD/DVD, USB u otro) y proceden a iniciar el sistema operativo, cediéndole el control de la computadora.

6.1 BIOS (Basic Input Output System)
---------------------------------------------------------------------

La función principal del BIOS (Sistema Básico de Entrada y Salida) es la de iniciar los componentes de hardware y lanzar el sistema operativo de una computadora cuando se encienda.

Los sistemas con BIOS sólo soportan hasta cuatro particiones y discos duros de una capacidad máxima de 2,2 TB. Eso es porque utilizan el esquema de particiones MBR. 

.. figure:: images/u2_BIOS_setup_utility.png
    :align: center
    :scale: 225 %
    

6.2 UEFI (Unified Extensible Firmware Interface)
---------------------------------------------------------------------

La Interfaz de Firmware Extensible Unificada o UEFI es el firmware sucesor del BIOS. Todo lo que hace el BIOS, también lo hace la UEFI. Aunque ésta última tiene otras funciones adicionales y mejoras, como una interfaz gráfica mucho más moderna, un sistema de inicio seguro, una mayor velocidad de arranque y el soporte para discos duros de más de 2 TB.
    
.. figure:: images/u2_UEFI.png
    :align: center
    :scale: 33 %


    
Bibliografía
================================================================

*“Partición de disco”*. Wikipedia. Online: https://es.wikipedia.org/wiki/Partici%C3%B3n_de_disco

*“Apéndice A. Introducción a la creación de particiones”*. Guía de instalación de Red Hat Enterprise Linux 7. Online: https://access.redhat.com/documentation/es-es/red_hat_enterprise_linux/7/html/installation_guide/chap-making-media

*“Discos MBR y GPT, diferencias entre los dos estándares de la actualidad”*. Online: https://www.profesionalreview.com/2018/03/10/discos-mbr-y-gtp-diferencias-entre-los-dos-estandares-de-la-actualidad/

*“MBR y GPT, todo sobre estos dos estilos de particiones de discos”*. Online: https://www.softzone.es/2016/03/25/mbr-gpt-estos-dos-estilos-particiones-discos/

*“UEFI y BIOS: ¿cuales son las diferencias?”*. Online: https://www.xataka.com/basics/uefi-y-bios-cuales-son-las-diferencias



